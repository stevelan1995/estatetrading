import re
import os
import codecs
import pprint
import requests

import os.path as path
import pandas as pd
import numpy as np
from bs4 import BeautifulSoup
from collections import OrderedDict
from pandas import DataFrame

pp = pprint.PrettyPrinter(indent=4)

def locate_sibling(tag_name, sib_tag, data_area, heading_text):
    node = data_area.find(tag_name, text=heading_text)
    node_parent = node.find_parent()
    sib = node_parent.find_next_siblings(sib_tag)
    return sib

def fill_price(data_area, property_dict):
    # Find price
    for s in data_area.find_all('span', {'class':'field'}):
        if re.match('\$[0-9]{1,3}(,[0-9]+)+', s.text):
            property_dict['Price'] = s.text
            break
    return property_dict

def fill_status(data_area, property_dict):
    property_dict['Status'] = data_area.find('span', {'class':re.compile('Status_\S')}).text
    return property_dict

def fill_address(data_area, property_dict):
    property_dict['Address'] = data_area.find('a', {'title':'View Map'}).text
    return property_dict

def fill_room_summary(data_area, property_dict):
    rooms = ['Bedrooms', 'Baths', 'Half_Bath']
    levels = ['Upper', 'Main', 'Lower', 'Total']
    count = ['Total_Bedrooms', 'Total_Baths', 'Total_Half_Bath']


    for floor in levels:
        room_count = [int(cnt.text) for cnt in locate_sibling('span', "td", data_area, floor)[:3]]

        for r, c in zip(rooms, room_count):
            key = "{}_{}".format(floor, r)
            property_dict[key] = c

    return property_dict

def fill_directions(data_area, property_dict):
    direction_desc = locate_sibling(tag_name="span", sib_tag='td', data_area=data_area, heading_text='Directions: ')[0].text
    property_dict['Directions'] = direction_desc

    return property_dict

def fill_property_features(data_area, property_dict):
    feature_node_sibs = [node.find_all('span', {'class': ['label', 'wrapped-field', 'formula field ', 'field']}) for node in data_area.find('span', text='FEATURES').find_parents('td', recursive=False) if len(node) > 0]
    feature_descriptions = [node.getText() for node in feature_node_sibs[1] if len(node) > 0]

    # For now, we assume any text with comma at the end is a title
    # use regex to find them
    # if a heading has value, the one next to it is the value we need, otherwise it is empty                   
    for i in range(0, len(feature_descriptions)-1):
        if re.match(r'[#\w\s\d]+:', feature_descriptions[i]) and not re.match(r'[\w\s\d]+:', feature_descriptions[i+1]):
            key = feature_descriptions[i].strip(': ')
            description = feature_descriptions[i+1]
            property_dict[key] = description
    
    return property_dict

def fill_school_info(data_area, property_dict):
    property_dict['Elementary_School'] = property_dict.pop('Elem')
    property_dict['Middle_School'] = property_dict.pop('Middle')
    property_dict['High_School'] = property_dict.pop('High')

    return property_dict



def fill_public_private_info(data_area, property_dict):
    private_node_sib = locate_sibling(tag_name='span', sib_tag='td', data_area=data_area, heading_text='Private:')
    private_msg = private_node_sib[0].getText()

    public_node_sib = locate_sibling(tag_name='span', sib_tag='td', data_area=data_area, heading_text='Public:')
    public_msg = public_node_sib[0].getText()

    property_dict['Public'] = public_msg
    property_dict['Private'] = private_msg

    return property_dict


def clean_useless_key(data_area, property_dict):
    room_name = ['Bdrms', 'Baths', 'Hlf Bth']
    rooms = ['Bedrooms', 'Baths', 'Half_Bath']
    levels = ['Upper', 'Main', 'Lower', 'Total']
    count = ['Total_Bedrooms', 'Total_Baths', 'Total_Half_Bath']

    property_dict.pop('Lvls')
    property_dict.pop('Total')
    property_dict.pop('FEATURES')
    property_dict.pop('SCHOOLS')

    for rk, lk in zip(room_name, levels):
        property_dict.pop(rk)
        property_dict.pop(lk)


    return property_dict

def fill_missing_variables(data_area, property_dict):
    # all unstructured variable values get filled here
    # fill in some known unstructured data
    """
    SCHOOLS, Directions:, Public:
    Private:, FEATURES, Bedroom:, Dining: , Master Bath:, Kitchen: , Rooms: , Appliances: 
    Laundry: ,Basement: ,Interior: ,Accessibility: ,Exterior: ,Lot Size: ,Lot Desc: ,Parking: 
    Roof: , Heating: , Cooling: , Water: , Sewer Desc: , Pool: 
    # Fire Places: ,Fireplace: ,HERS Index: ,Grn Bld Cert: ,Dock: ,Boathouse:
    """
    property_dict = fill_status(data_area, property_dict)
    property_dict = fill_price(data_area, property_dict)
    property_dict = fill_address(data_area, property_dict)
    property_dict = fill_room_summary(data_area, property_dict)
    property_dict = fill_directions(data_area, property_dict)
    property_dict = fill_property_features(data_area, property_dict)
    property_dict = clean_useless_key(data_area, property_dict)
    property_dict = fill_public_private_info(data_area, property_dict)
    property_dict = fill_school_info(data_area, property_dict)
    
    return property_dict

def fill_structured_variables(data_area, property_dict):
    """ This method assumes that structured data are organized by a label with its value field sibling.
    A simple regex match should extract such kind of data. For other relatively unstructured data, 
    record their names and fill them with nan. They will be handled by a specialized unstructured
    data extraction method.

    """
    heading_labels = [head_label for head_label in data_area.find_all('span', {'class':'label'}) if '|' not in head_label.text][1:]

    for label in heading_labels:
        col_name = label.text
        sib = label.find_next_siblings()

        col_name_cleaned = re.sub(r'([^\s\w/#]|_)+', '', col_name).rstrip()

        if len(sib) > 0:
            entry_value = sib[0].text.lstrip()
            property_dict[col_name_cleaned] = entry_value
        else:
            property_dict[col_name_cleaned] = np.nan

    return property_dict

def count_effective_data(property_dict):
    effective_data = 0
    
    for v in property_dict.values():
        if type(v) is str:
            if len(v) > 0:
                effective_data += 1
        elif type(v) is float and not np.isnan(v):
            effective_data += 1
        elif v is not None and v != 'None':
            effective_data += 1
        else:
            effective_data += 0
    
    return effective_data

def save_result(input_data, report_name, save_path):
    merged_data = pd.concat(input_data, axis=0, ignore_index=True, verify_integrity=True)
    merged_data.to_csv(save_path+report_name, index=False)
    
    pp.pprint(merged_data)
    # pp.pprint("Data Saved!")


def extract_column_names(column_container, table_data):
    """ Initialize extraction data by extracting column names with empty list.

    """

    column_header = extract_children(column_container, index=0)
    columns = extract_children(column_header, start=1)

    for col in columns:
        table_data[col.text] = []

    return table_data

def fill_table_data(table_body, table_data):
    # pp.pprint(table_data.keys())

    for row in table_body.children:
        # Alert! Check extracted data type!
        # Data should be tags only. If you see something else, you make a mistake!
        data_entries = [e for e in extract_children(row, start=1) if e != '\n']
        # print(type(data_entries[0]))
        # pp.pprint(data_entries)

        for key, entry in zip(table_data.keys(), data_entries):
            # print(entry.text)
            if entry.text == '':
                table_data[key].append(np.nan)
            else:
                table_data[key].append(entry.text)

    return table_data

def extract_children(element, index=None, start=None, end=None):
    """ Extract the children of the provided element.

    If index, start, and end are all none, element itself is returned.
    If only index is given, it is equivalent to element.children[index].
    If either start or end is given, it is equivalent to element.children[start:end]
    Index and start, end cannot be given at the same time, otherwise it 
    is equivalent to the first case.

    """

    if (index is None and start is None and end is None) or \
       (index is not None and start is not None) or \
       (index is not None and end is not None):
        return element
    elif start is not None and end is not None:
        return list(element.children)[start: end]
    elif start is not None:
        return list(element.children)[start: ]
    elif end is not None:
        return list(element.children)[:end]
    elif index is not None:
        return list(element.children)[index]
    else:
        return element


def extract_single_page_from_file(save_path, report_name, test_page_path, var_tables, total_page):
    full_table_data = {}
    dataset = []

    for p in range(total_page):
        full_page_data = []

        for raw_page in var_tables:
            table_data = OrderedDict()
            page_data_name = raw_page.split('.')[0]+"_scraped.csv"
            
            with codecs.open(test_page_path+str(p)+'/'+'X_{}'.format(p)+raw_page, 'r') as test_page:
                print("Reading {}".format(test_page_path+str(p)+'/'+'X_%i'%p+raw_page))
                page_src = test_page.read()
                soup = BeautifulSoup(page_src, "lxml")

                data_area = extract_children(soup.find(id="m_pnlDisplay"), index=1)
                header_container, table_body = extract_children(data_area, start=1)
                table_data = extract_column_names(header_container, table_data)
                table_data = fill_table_data(table_body, table_data)
                table_data_df = DataFrame(table_data, index=None)
                full_page_data.append(table_data_df)

        page_data_df = pd.concat(full_page_data, axis=1, verify_integrity=True)
        dataset.append(page_data_df)

    dataset[0].to_csv(save_path+"page_0.csv", index = False)
    dataset[1].to_csv(save_path+"page_1.csv", index = False)

    save_result(dataset, report_name, save_path)

    # save_result(full_table_data, report_name, save_path)




if __name__ == '__main__':
    test_page_path = path.expanduser('~/Desktop/real_estate_data/raw_webpage/listing_page/')
    save_path = path.expanduser('~/Desktop/real_estate_data/csv_data/scraped_list/')
    total_page = 2

    var_tables = ['{}.html'.format(i) for i in range(0,7)]
    print(var_tables)
    offline_report = 'full_variable_data.csv'

    extract_single_page_from_file(save_path, offline_report, test_page_path, var_tables, total_page)


