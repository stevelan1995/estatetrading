import json
import os
import os.path as path
import time
import glob
import shutil
import selenium.webdriver.support.ui as ui
from selenium.webdriver.common.keys import Keys
from selenium import webdriver
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.action_chains import ActionChains

import pprint
pp = pprint.PrettyPrinter(indent=4)


def login(browser, config_path):
    with open(config_path+'login_info.json', 'r') as f:
        login_data = json.load(f)['atlanta']

    # automatically login to fmls database 
    browser.get("https://www.fmls.com/")
    username_input = browser.find_element_by_id(login_data['username_id'])
    passwd_input = browser.find_element_by_name(login_data['pass_name'])
    login_button = browser.find_element_by_tag_name('button')
    
    # Enter username and password
    username_input.send_keys(login_data['username'])
    passwd_input.send_keys(login_data['passwd'])
    login_button.click()
    time.sleep(2)

def access_db(browser, config_path):
    with open(config_path+'sess_info.json', 'r') as f:
        interactive_sess_data = json.load(f)

    product_dropdown = browser.find_element_by_xpath(interactive_sess_data['product_dropdown'])
    product_dropdown.click()
    time.sleep(2)
    matrix_db_link = browser.find_element_by_xpath(interactive_sess_data['Matrix_db'])
    matrix_db_link.click()

    browser.get(interactive_sess_data['search_page'])
    time.sleep(1)
    browser.switch_to_window(browser.window_handles[-1])
    time.sleep(1)
    
    time.sleep(2)
    search_btn = browser.find_element_by_xpath(interactive_sess_data['search_button'])
    search_btn.click()


def main():

    estate_data_path = path.expanduser("~/Desktop/real_estate_data/fmls/")
    config_path = path.expanduser("~/Desktop/estatetrading/configs/")
    
    chrome_options = webdriver.ChromeOptions()
    prefs = {'download.default_directory' : estate_data_path}
    chrome_options.add_experimental_option('prefs', prefs)
    browser = webdriver.Chrome(chrome_options=chrome_options)    
    
    login(browser, config_path)
    access_db(browser, config_path)
    build_complete_table(browser, config_path)

    time.sleep(20)
    browser.close()


if __name__ == '__main__':
    main()













