import schedule
import time

def job(a, b):
    print(a)
    print(b)

    return a, b

def my_job():
    # This job will execute every 5 to 10 seconds.
    print('Foo')


schedule.every().day.at("10:30").do(job)

while True:
    schedule.run_pending()
    # time.sleep(1) # wait one minute