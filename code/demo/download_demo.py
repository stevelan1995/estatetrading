import json
import os
import os.path as path
import time
import glob
import shutil
import selenium.webdriver.support.ui as ui
from selenium.webdriver.common.keys import Keys
from selenium import webdriver
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.action_chains import ActionChains

def login(browser, config_path):
    with open(config_path+'login_info.json', 'r') as f:
        login_data = json.load(f)

    # automatically login to fmls database 
    browser.get("https://www.fmls.com/")
    username_input = browser.find_element_by_id(login_data['fmls_username_id'])
    passwd_input = browser.find_element_by_name(login_data['fmls_pass_name'])
    login_button = browser.find_element_by_tag_name('button')
    
    # Enter username and password
    username_input.send_keys(login_data['username'])
    passwd_input.send_keys(login_data['passwd'])
    login_button.click()
    time.sleep(2)

def access_db(browser, config_path):
    with open(config_path+'sess_info.json', 'r') as f:
        interactive_sess_data = json.load(f)

    product_dropdown = browser.find_element_by_xpath(interactive_sess_data['product_dropdown'])
    product_dropdown.click()
    time.sleep(2)
    matrix_db_link = browser.find_element_by_xpath(interactive_sess_data['Matrix_db'])
    matrix_db_link.click()

    browser.get(interactive_sess_data['search_page'])
    time.sleep(1)
    browser.switch_to_window(browser.window_handles[-1])
    time.sleep(1)

    active_chk = browser.find_element_by_xpath(interactive_sess_data['active_checkbox'])
    active_chk.click()
    time.sleep(0.5)

    day_range = browser.find_element_by_xpath(interactive_sess_data['active_day_range'])
    day_range.send_keys('5')
    time.sleep(1)

    day_range.send_keys(Keys.ENTER)

    time.sleep(2)

def duplicate_tab(browser, config_path):
    # browser.switch_to_window(browser.window_handles[-1])
    duplicate_url = browser.current_url
    print(duplicate_url)

    browser.execute_script("window.open('about:blank');")    # time.sleep(0.5)
    time.sleep(1)    
    browser.switch_to_window(browser.window_handles[-1])

    browser.get(duplicate_url)

def move_to_download_folder(save_path, new_path, extension):
    for path in glob.glob(save_path+extension):
        f_name = path.split('/')[-1]
        ## Create new file name

        file_destination = new_path+f_name
        print("Move from {} to {}".format(path, file_destination))
        shutil.move(path, file_destination)

def make_save_folder(save_path, data_n):
    if not os.path.exists(save_path+data_n):
        os.makedirs(save_path+data_n)
    
def download_data(browser, config_path, save_path):
    with open(config_path+'sess_info.json', 'r') as f:
        interactive_sess_data = json.load(f)

    pages_link = browser.find_element_by_xpath(interactive_sess_data['paging'])
    pages = pages_link.find_elements_by_tag_name('a')[1:-1]
    
    for i, page in zip(range(len(pages)), pages):
        duplicate_tab(browser, config_path)
        make_save_folder(save_path, str(i))

        for j in range(3):
            select_all = browser.find_element_by_xpath(interactive_sess_data['select_all_checkbox'])
            select_all.click()
            time.sleep(3)

        download_button = browser.find_element_by_xpath(interactive_sess_data['download_button'])
        download_button.click()
        time.sleep(3)

        export_button = browser.find_element_by_xpath(interactive_sess_data['export_csv'])
        export_button.click()
        time.sleep(3)

        # move to the directory I want to save
        time.sleep(3)
        move_to_download_folder(save_path, save_path+str(i)+'/', '*.csv')

        for view_opt in browser.find_elements_by_tag_name('option')[:-1]:
            view_opt.click()
            time.sleep(0.1)

            export_button.click()
            time.sleep(0.5)
        
        browser.close()
        browser.switch_to_window(browser.window_handles[-1])
        page.click()

def main():

    estate_data_path = path.expanduser("~/Desktop/real_estate_data/fmls/")
    config_path = path.expanduser("~/Desktop/estatetrading/configs/")
    
    chrome_options = webdriver.ChromeOptions()
    prefs = {'download.default_directory' : estate_data_path}
    chrome_options.add_experimental_option('prefs', prefs)
    browser = webdriver.Chrome(chrome_options=chrome_options)    
    
    login(browser, config_path)
    access_db(browser, config_path)
    download_data(browser, config_path, estate_data_path)

    time.sleep(5)
    browser.close()


if __name__ == '__main__':
    main()













