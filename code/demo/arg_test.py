import argparse

def setup_args():
    def _str_to_bool(s):
        """Convert string to bool (in argparse context)."""
        if s.lower() not in ['true', 'false']:
            raise ValueError('Argument needs to be a '
                             'boolean, got {}'.format(s))
        return {'true': True, 'false': False}[s.lower()]

    status_help = """Status of the listed properties.
                    Atlanta: [active, backup_contract_call, cancelled, closed_sale, pending_sale, rented, temp_off_market, withdrawn, expired],
                    Philadelphia: []
                """

    parser = argparse.ArgumentParser(description='Scraper for extracting data from MLS databases.')
    parser.add_argument('database', '--db', type=str,
                        help='FMLS database to scrape from. Available (all lower cases): [atlanta, philadelphia, south_florida]',
                        choices=['atlanta, philadelphia, south_florida']
    )
    parser.add_argument('-s','--status', action='store_const')    
    parser.add_argument('-sd', '--start_date', action='store_const')    
    parser.add_argument('-ed', '--end_date', action='store_const')    
    parser.add_argument('-sr', '--silent_run', action='store_const', default=True)    
    parser.add_argument('--export_path', action='store_const')    
    args = parser.parse_args()


def main():
    db_name = 'atlanta'
    config_path = path.expanduser("~/Desktop/estatetrading/configs/")
    export_path = path.expanduser("~/Desktop/mls_data/estate_trading/data/")

    status = 'active'
    category = 'rental'

    hide_browser = True

    # we get data by go back in time
    historical_end_date = '05/25/2018'
    start_day = '05/27/2018'

    data_set_name = '{}_{}_{}_query_{}.csv'.format(db_name, category, status, CURRENT_TIME)


if __name__ == '__main__':
    main()

