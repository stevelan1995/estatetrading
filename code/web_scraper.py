import re
import os
import pprint

import os.path as path
import pandas as pd
import numpy as np
from bs4 import BeautifulSoup
from collections import OrderedDict
from pandas import DataFrame
from time import gmtime, strftime
from selenium.common.exceptions import NoSuchElementException, StaleElementReferenceException


class WebScraper:
    """docstring for Scraper"""
    def __init__(self):
        pass
    
    def _extract_column_names(self, column_container, table_data):
        """ Initialize extraction data by extracting column names with empty list.

        """
        column_header = self._extract_children(column_container, index=0)
        columns = self._extract_children(column_header, start=1)

        for col in columns:
            if col != u'\xa0' and col !='\n' and col != " ":
                table_data[col.text] = []
            else:
                print("Found hidden invalid character!")

        return table_data

    def _fill_table_data(self, table_body, table_data):

        for row in table_body.children:
            data_entries = [e for e in self._extract_children(row, start=1) if e != '\n']

            for key, entry in zip(table_data.keys(), data_entries):
                if entry.text == '':
                    table_data[key].append(np.nan)
                else:
                    table_data[key].append(entry.text)

        return table_data

    def _extract_children(self, element, index=None, start=None, end=None):
        """ Extract the children of the provided element.

        If index, start, and end are all none, element itself is returned.
        If only index is given, it is equivalent to element.children[index].
        If either start or end is given, it is equivalent to element.children[start:end]
        Index and start, end cannot be given at the same time, otherwise it 
        is equivalent to the first case.

        """

        if (index is None and start is None and end is None) or \
           (index is not None and start is not None) or \
           (index is not None and end is not None):
            return element
        elif start is not None and end is not None:
            return list(element.children)[start: end]
        elif start is not None:
            return list(element.children)[start: ]
        elif end is not None:
            return list(element.children)[:end]
        elif index is not None:
            return list(element.children)[index]
        else:
            return element


    def _extract_variable_table(self, page_source):
        """ Extract variables from raw html containing variable tables like X1.
        Returns a dataframe representation. 

        """
        table_data = OrderedDict()
        soup = BeautifulSoup(page_source, "lxml")

        data_area = self._extract_children(soup.find(id="m_pnlDisplay"), index=1)
        header_container, table_body = self._extract_children(data_area, start=1)
        table_data = self._extract_column_names(header_container, table_data)
        table_data = self._fill_table_data(table_body, table_data)
        table_data_df = DataFrame(table_data, index=None)
        
        return table_data_df
 
    def _drop_duplicated(self, df):
        """ Assuming the dataframe somehow contrains duplicates that looks like
        something.1. Drop it. Also it should not contain illegal characters.

        """

        for col_name in df.columns:
            if re.findall('\.\d', col_name):
                df = df.drop(col_name, axis=1)
            elif col_name == u'\xa0':
                df = df.drop( u'\xa0', axis=1)

        return df

    def extract_full_page(self, raw_variable_table):
        """ Extract a full page of variable tables containing about 250-280 variables.
        Takes a list of raw html containing all the variables and convert into 
        dataframe.

        """
        full_page = []

        for raw_variable in raw_variable_table:
            var_table_df = self._extract_variable_table(raw_variable)
            full_page.append(var_table_df)

        # Concat by column, and perform sanity check.
        full_page_df = self._drop_duplicated(pd.concat(full_page, axis=1))

        return full_page_df