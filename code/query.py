import re
import time
import json
import selenium.webdriver.support.ui as ui
from selenium.webdriver.common.keys import Keys
from selenium import webdriver
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By

from selenium.common.exceptions import NoSuchElementException, StaleElementReferenceException, ElementNotVisibleException, WebDriverException


class Query():
    """Query helps us to submit basic searches to narrow down
    the range of data. The very first version should be able to
    go to atlanta FMLS and select either residential or rental, and
    select one of two house types, then select any one of the status.

    """

    def __init__(self, browser, config_path):
        self.browser = browser
        self.db_name = ""
        self._init_query_config(config_path)


    def _init_query_config(self, config_path):
        with open(config_path+'query_info.json', 'r') as f:
            self.db_session_data = json.load(f)

    def set_db(self, db):
        self.db_name = db


    def _uncheck_default_status(self, market_type):
        """ The Philly database has default status checked. We don't want that.

        """
        op_successful = True
        default_active_status_found = False
        default_active_no_show_status_found = False

        if not (self.db_name == 'atlanta' and market_type == 'residential'):
            while not default_active_status_found:
                try:
                    time.sleep(0.5)
                    default_active = self.browser.find_element_by_xpath(self.current_session[market_type+"_active"])
                    self._click(default_active)
                    time.sleep(0.5)
                    default_active_status_found = True
                except NoSuchElementException:
                    print("Default active status not found.")
                    self.browser.refresh()
                    time.sleep(0.5)
                except Exception:
                    print("Something is wrong. Skip and reinitiate search. Exception: {}".format(Exception))
                    op_successful = False
                    return op_successful

        if self.db_name == 'philadelphia':
            while not default_active_no_show_status_found:
                try:
                    time.sleep(0.5)
                    default_active_no_show = self.browser.find_element_by_xpath(self.current_session[market_type+"_active_no_showing"])
                    self._click(default_active_no_show)
                    time.sleep(0.5)
                    default_active_no_show_status_found = True
                except NoSuchElementException:
                    print("Default active no show status not found.")
                    self.browser.refresh()
                    time.sleep(0.5)
                except Exception:
                    print("Something is wrong. Skip and reinitiate search. Exception: {}".format(Exception))
                    op_successful = False
                    return op_successful

        return op_successful

    def _click(self, element):
        self.browser.execute_script("arguments[0].click();", element)


    def submit_query(self, market_type, property_type, status, look_back=None, start=None, end=None):
        # go to search tab
        self.current_session =  self.db_session_data[self.db_name]
        query_successful = True

        search_tab_found = False
        search_page_found = False
        status_found = False
        status_date_range_found = False
        entry_count_found = False
        search_btn_found = False
        entry_count = 0

        self.browser.get(self.current_session[market_type+"_search_page"])
        time.sleep(1)

        success = self._uncheck_default_status(market_type)

        if not success:
            return False

        while not status_found:
            try:
                if market_type+'_'+status in self.current_session:
                    time.sleep(1)
                    status_chk = self.browser.find_element_by_xpath(self.current_session[market_type+'_'+status])
                    self._click(status_chk)

                    time.sleep(0.5)
                    status_found = True
                else:
                    print("Incorrect query: requested market_type or status not available for this region.")
                    query_successful = False
                    return query_successful, entry_count

            except NoSuchElementException:
                print("Element on the status page is not found.")
                time.sleep(1)
            except ElementNotVisibleException:
                print("Element on the status page is not visible.")
                self.browser.refresh()

                time.sleep(1)
            except WebDriverException:
                self.browser.execute_script("arguments[0].click();", status_chk)
            # except Exception:
            #     pass
                # self.browser.refresh()

                # print("Status not found. Skip and reinitiate search. Exception: {}".format(Exception))
                # query_successful = False
                # return query_successful, entry_count

        while not status_date_range_found:
            try:
                time.sleep(0.5)
                status_date_range = self.browser.find_element_by_xpath(self.current_session[market_type+'_'+status+"_input"])
                status_date_range.clear()
                status_date_range_found = True
            except NoSuchElementException:
                print("Element on the status date range page is not found.")
                time.sleep(0.5)
            except Exception:
                print("Status date range not found. Skip and reinitiate search. Exception: {}".format(Exception))
                query_successful = False
                return query_successful, entry_count

        
        if start is not None and end is not None:
            date_str = "{}".format(end_date.replace('-', '/'))
            status_date_range.send_keys(date_str)
        elif look_back is not None:
            # print(look_back, type(look_back))
            status_date_range.send_keys(str(look_back.days))
        else:
            pass

        time.sleep(2)

        while not entry_count_found:
            try:
                time.sleep(1)
                entry_count_btn = self.browser.find_element_by_xpath(self.current_session["entry_count"])
                count_text = entry_count_btn.text
                print(count_text)

                if (not re.match('\?', count_text)) and (not re.match('5000\+',count_text)):
                    entry_count = int(re.findall('\d+', entry_count_btn.text)[0])

                entry_count_found = True
            except NoSuchElementException:
                print("Cannot find entry count")
                # self.browser.refresh()
                time.sleep(1)
            except Exception:
                print("Entry count not found. Skip and reinitiate search. Exception: {}".format(Exception))
                query_successful = False
                return query_successful, entry_count
                
        if entry_count > 0:
            while not search_btn_found:
                try:
                    time.sleep(1)
                    submit_search = self.browser.find_element_by_xpath(self.current_session["submit_search"])
                    self._click(submit_search)
                    time.sleep(1)
                    search_btn_found = True
                except NoSuchElementException:
                    print("Cannot find search button")
                    # self.browser.refresh()
                    time.sleep(1)
                except Exception:
                    print("Search button not found. Skip and reinitiate search. Exception: {}".format(Exception))
                    query_successful = False
                    return query_successful, entry_count
        
        return query_successful, entry_count

    def show_entry_count(self):
        element_found = False
        attempt = 0
        threshold = 10
        entry_count = 0

        # if element not found, try to redo the search
        while attempt < threshold and not element_found :
            try:
                time.sleep(1)
                q_count = self.browser.find_element_by_xpath(self.current_session['query_count']).text
                entry_count = int(re.findall('\d+', q_count)[0])
                element_found = True
            except NoSuchElementException:
                print("Search entry is not found.")
                time.sleep(1)
                attempt += 1


        return entry_count




