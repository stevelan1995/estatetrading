"""
Data Scraping System Prototype: Scraping three FMLS databases

This prototype consists of three modules:
1. Operator
2. WebScraper
3. Query

Operator simulates human operation on the self.browser and is responsible for submiting query and
browsing pages.

WebScraper takes a data listing page and scrape data from it

Query is responsible for choosing the correct filter. 

Note: Aggressive scraping may alert the database administrator

Scraping the whole database cannot be done at once, and needs to be broken 
down into sessions. Each session simulates browsing the database by n minutes.
Each session is stored as a session checkpoint, and the scraper can start
from the lastest checkpoint to continue scraping. Scraping operation should be
as stealthy as possible. 

By default, data scraper updates the last checkpoint every one minute so it can 
recover from the past one minute if get interrupted for any reason.

"""

import re
import argparse
import json
import os
import os.path as path
import time
import glob

import random
import pandas as pd
import schedule
import datetime
import numpy as np
import subprocess
from subprocess import call
import platform
from selenium.common.exceptions import NoSuchElementException, StaleElementReferenceException

from apscheduler.schedulers.background import BackgroundScheduler
from time import gmtime, strftime

import pprint
pp = pprint.PrettyPrinter(indent=4)

from code.operator import Operator
from code.web_scraper import WebScraper

class DBScraper():
    """ Main driver class for scraping MLS databases.

    """

    def __init__(self, db_name, config_path, export_path, dataset_name, hide_browser):
        self.db_name = db_name
        self.operator = Operator(db_name, config_path, export_path, hide_browser)
        self.scraper = WebScraper()
        self._init_config(config_path)
        self.export_path = export_path
        self.dataset_name = dataset_name
        self.total_lines = 0
        self.total_variables = self.interactive_sess_data[db_name]["total_variables"]

    def _init_config(self, config_path):
        with open(config_path+'sess_info.json', 'r') as f:
            self.interactive_sess_data = json.load(f)

        with open(config_path+'columns.json', 'r') as f:
            all_column_names = json.load(f)
            self.column_names = [s.strip('\n') for s in all_column_names[self.db_name]]

        with open(config_path+"email_pattern.txt", "r") as f:
            email_pattern_str = f.read()
            self.email_pattern = re.compile(email_pattern_str)

    def save_result(self, merged_data, part_no=None):
        if not path.exists(self.export_path):
            os.makedirs(self.export_path)

        if part_no is not None:
            print("Save partial data as {}".format(self.export_path+self.dataset_name+str(part_no)+".csv"))
            merged_data.to_csv(self.export_path+self.dataset_name+str(part_no)+".csv", index=False)
        else:
            merged_data.to_csv(self.export_path+self.dataset_name, index=False)

        print("Data Saved!")

        # Perform a simple sanity check to see whether dimension matches up
        query_dim = merged_data.shape
        expected_dim = (self.total_lines, self.total_variables)

        if query_dim != expected_dim:
            print("Expected dimension is {}.\nActual dimesion is {}".format(query_dim, expected_dim))


    def save_html(self, page_src, page_name, table_number):
        """ Save html file for testing and analysis purposes"""
        if not path.isdir(self.export_path+page_name+'/'):
            os.makedirs(self.export_path+page_name+'/')

        with open(self.export_path+page_name+'/'+table_number+'.html', "w") as page_file:
            print("write {} to {}".format(table_number+'.html', self.export_path+page_name+'/'))
            page_file.write(page_src)


    def _concat_pages(self, page_dfs):
        """ Take a list of dfs containing pages 
        and merge each df to one df by row. One query should
        return a df shape about (5000, 260).

        Pandas sanity check enabled for testing purposes. 
        For now, there is no absolute garuntee about the correctness
        of the data even though I spend a lot effort to reach this objective.
        """
        try:
            full_merged_query = pd.concat(page_dfs, axis=0, ignore_index=True, verify_integrity=True)
        except ValueError:
            full_merged_query = pd.DataFrame()
            for i, page in enumerate(page_dfs):
                if page.shape[1] == self.total_variables: 
                    # skip faulty page
                    try:
                        full_merged_query = pd.concat([full_merged_query, page], axis=0, ignore_index=True)
                    except ValueError:
                        pass
                else:
                    print("Page {} is faulty. Save as separate file.".format(i))
                    page.to_csv(self.export_path+"faulty_{}.csv".format(i), index=False)

        except Exception:
            full_merged_query = pd.DataFrame()
            for i, page in enumerate(page_dfs):
                if page.shape[1] == self.total_variables: 
                    # skip faulty page
                    try:
                        full_merged_query = pd.concat([full_merged_query, page], axis=0, ignore_index=True)
                    except ValueError:
                        pass
                else:
                    print("Page {} is faulty. Save as separate file.".format(i))
                    page.to_csv(self.export_path+"faulty_{}.csv".format(i), index=False)

        return full_merged_query

    def _drop_duplicated(self, df):
        """ Assuming the dataframe somehow contrains duplicates that looks like
        something.1. Drop it. Also it should not contain illegal characters.

        """

        for col_name in df.columns:
            if re.findall('\.\d', col_name):
                df = df.drop(col_name, axis=1)
            elif col_name == '\xa0':
                df = df.drop(col_name, axis=1)

        # df = df.drop_duplicates()
        try:
            df = df[self.column_names]
        except Exception as e:
            pass

        return df

    def update_export_path(self, db_name, market_type):
        query_folder = "query_{}".format(CURRENT_TIME)
        self.export_path = self.export_path+"{}/{}/{}/".format(db_name, market_type, query_folder)
        print("Data is saved at {}".format(self.export_path))
        
        if not path.isdir(self.export_path):
            os.makedirs(self.export_path)


    def _create_fail_safe(self):
        # build a temporary cache folder to be more robust against network error
        current_time = datetime.datetime.now().strftime("%Y%m%d_%H%M%S")
        if not path.isdir(self.export_path+'cache/'+current_time+"_cache"+'/'):
            os.makedirs(self.export_path+'cache/'+current_time+"_cache"+'/')
            self.cache_folder = self.export_path+'cache/'+current_time+"_cache"+'/'
            print("Page caches are stored at {}".format(self.cache_folder))
        
        if not path.isdir(self.export_path+'abnormal_files/'+current_time+"_error"+'/'):
            os.makedirs(self.export_path+'abnormal_files/'+current_time+"_error"+'/')
            self.abnormal_folder = self.export_path+'abnormal_files/'+current_time+"_error"+'/'
            print("Dirty fail safe backups are stored at {}".format(self.abnormal_folder))

    def _extract_email_address(self, column):
        email_col = {"Agent Email": []}

        for r in column.itertuples():
            if len(r) == 2:
                agent_remark = r[1]

                if type(agent_remark) is str:
                    matched = re.findall(self.email_pattern, agent_remark)
                    email = ""
                    if len(matched) > 1:
                        for addr in matched:
                            if type(addr) is str:
                                email += addr + "; "
                    elif len(matched) == 1:
                        email = matched[0]
                    
                    email_col['Agent Email'].append(email)
                else:
                    email_col['Agent Email'].append(np.nan)
            else:
                print("Abnormal remark row: {}".format(r))

        return pd.DataFrame(email_col)


    def _extract_agent_email(self, df):
        if self.db_name == 'atlanta':
            remark = df.filter(regex='Private Remarks')
        elif self.db_name == 'philadelphia':
            remark = df.filter(regex='Agent Remarks')
        
        agent_email = self._extract_email_address(remark)
        df = pd.concat([df, agent_email], axis=1)

        return df, 1

    def _post_process(self, df):
        new_cols = 0
        
        if self.db_name != "south_florida":
            # assume there are hidden emails in private and public remarks
            df, new_cols = self._extract_agent_email(df)

        return df, new_cols

    def start_scrape(self, market_type, property_types, status, start_date, end_date,
                     save_step=0, sleep_step=None, sleep_time_range=None):
        """ Constant online scraping
        From the last day to go to start day. Each query contains one day of data.

        """
        search_attempt = 0
        max_retry = 10
        part_number = 0

        self.operator.login()
        self.operator.access_db(market_type)
        date_range = pd.date_range(start=start_date, end=end_date)

        print("Today is {}".format(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")))
        print("Scraping date from {} to {}".format(end_date, start_date))
        print("Save step is {}".format(save_step))
        # save every n page
        full_query_data = []

        self.update_export_path(self.db_name, market_type)
        self._create_fail_safe()

        date_index = list(range(len(date_range)))

        # enable random day access
        if args['random_access']:
            random.shuffle(date_index)
        
        for step, day_idx in enumerate(date_index):
            if step > 0 and sleep_step is not None and sleep_time_range is not None and step % sleep_step == 0:
                min_sleep, max_sleep = sleep_time_range
                sleep_time = np.random.randint(min_sleep, max_sleep)
                self.operator.log_off()

                print("Log off and sleep for {} seconds.".format(sleep_time))
                time.sleep(sleep_time)
                self.operator.login()
                self.operator.access_db(market_type)

            search_successful = False
            search_attempt = 0
            day = date_range[day_idx]
            print("\nScraping {} data of status {} on day {}".format(market_type, status, day))
            look_back_days = datetime.datetime.now() - day
            print("There are {} pages in the memory.".format(len(full_query_data)))
            
            while search_attempt < max_retry and not search_successful:
                print("Search attempt {}".format(search_attempt))
                entries_found, search_successful = self.operator.execute_search(market_type, property_types, status, look_back_days=look_back_days)
                search_attempt += 1
                time.sleep(1)

            if search_successful:
                self.total_lines += entries_found
                print("Updated {} entries. We have {} entries now.".format(entries_found, self.total_lines))

                if not entries_found:
                    print("No entry returned for this search.")
                else:    
                    self.operator.show_max_row()
                    page_ends = False

                    x = 0
                    total_rows = 0
                    while not page_ends:
                        print("Scraping page {}".format(x))
                        
                        print("Save partial? {}".format(save_step > 0 and len(full_query_data) > 0 and len(full_query_data) % save_step == 0))
                        if save_step > 0 and len(full_query_data) > 0 and len(full_query_data) % save_step == 0:
                            print("Saving partial results.")
                            partial_data = [self._drop_duplicated(df) for df in full_query_data]
                            merged_data = self._concat_pages(partial_data)
                            self.save_result(merged_data, part_number)
                            part_number += 1
                            full_query_data = full_query_data[save_step:]

                        try:
                            column_match = False
                            while not column_match:
                                raw_variable_tables = self.operator.get_full_page()  
                                page_df = self.scraper.extract_full_page(raw_variable_tables)
                                page_df, added_cols = self._post_process(page_df)      
                            
                                if page_df.shape[1] == self.total_variables + added_cols:
                                    d_end = day.strftime("%Y-%m-%d_%H-%M-%S")
                                    page_df.to_csv(self.cache_folder+"{}_p{}.csv".format(d_end, x), index=False)                
                                    print("Saved page {} to {}\n".format(x, self.cache_folder))

                                    full_query_data.append(page_df)

                                    column_match = True
                                else:
                                    d_end = day.strftime("%Y-%m-%d_%H-%M-%S")
                                    page_df.to_csv(self.abnormal_folder+"{}_p{}.csv".format(time.time(), x), index=False)                
                                    print("Saved dirty page {} to {}\n".format(x, self.abnormal_folder))

                                if not column_match:
                                    # print("Validating column number: set(columns):{} == n_columns:{} : {}".format(len(set(page_df.columns)), len(page_df.columns), len(set(page_df.columns)) == page_df.shape[1]))
                                    print("Columns do not match with expected. Try again. Expected {}, Actual {}\n".format(self.total_variables, page_df.shape[1]))


                            print("Page {} has dimension {}\n".format(x, page_df.shape))
                            
                            x += 1
                            total_rows += page_df.shape[0]

                            self.operator.next_page()
                        except NoSuchElementException:
                            page_ends = True

                    # print("There are {} rows in the extracted data. Expected {}".format(sum([d.shape[0] for d in full_query_data]), self.total_lines))
        
        # if partial save not enabled or there are some pages left
        if save_step == 0 or len(full_query_data) > 0:
            if len(full_query_data) > 0:
                full_query_data = [self._drop_duplicated(df) for df in full_query_data]
                full_merged_data = self._concat_pages(full_query_data)
                
                if save_step > 0:
                    self.save_result(full_merged_data, part_number)
                else:
                    self.save_result(full_merged_data)
            else:
                print("This query returns no result. Please check your query.")

    def _subprocess_cmd(self, command):
        process = subprocess.Popen(command, stdout=subprocess.PIPE, shell=True)
        proc_stdout = process.communicate()[0].strip()
        print(proc_stdout)

    def auto_upload(self):
        """ For linux only because we don't have official google drive support
        Remove before delivery.

        """
        cmd = "cd ~/Desktop/mls_data/; grive"
        self._subprocess_cmd(cmd)


def setup_args():
    def _str_to_bool(s):
        """Convert string to bool (in argparse context)."""
        if s.lower() not in ['true', 'false']:
            raise ValueError('Argument needs to be a '
                             'boolean, got {}'.format(s))
        return {'true': True, 'false': False}[s.lower()]

    def _expand_path(s):
        return path.expanduser(s)

    def _str_to_seconds(s):
        h, m, sec = [int(t) for t in s.split(":")]
        sleep_time = datetime.timedelta(hours=h, minutes=m, seconds=sec)

        return sleep_time.total_seconds()

    status_help = """Status of the listed properties.\n
                    Atlanta: [active, cont_due_intel, cont_ko, cont_other, pending, pending_offer_approval, withdrawn, sold, expired],\n
                    Philadelphia: [active, active_no_showing, temp_off_market, settled, expired, expired_relisted, withdrawn_relisted, withdrawn, sold],\n
                    South Florida: [active, backup_contract_call, cancelled, closed_sale, rented, temp_off_market, withdrawn, expired]
                """

    DEFAULT_EXPORT_PATH = path.expanduser("~/Desktop/mls_data/estate_trading/data/")
    DEFAULT_CONFIG_PATH = path.expanduser("~/Desktop/estatetrading/configs/")

    parser = argparse.ArgumentParser(description='Scraper for extracting data from MLS databases.')
    parser.add_argument('--daily_active', type=_str_to_bool, choices=[True, False], default=False, help="Run a daily active feed on all databases. Can be used with scheduled time to specify the pulling time. Default is False")
    parser.add_argument('--db', type=str,
                        help='FMLS database to scrape from. Available (all lower cases): [atlanta, philadelphia, south_florida]',
                        choices=['atlanta', 'philadelphia', 'south_florida'],
                        default=None
    )
    parser.add_argument('-s', '--status', type=str, help=status_help, default=None)    
    parser.add_argument('-m', '--market', type=str, choices=['residential', 'rental'], help="Market type", default=None)    
    parser.add_argument('-sd', '--start_date', type=str, help="follow mm/dd/yyyy format. start_date <= end_date", default=None)    
    parser.add_argument('-ed', '--end_date', type=str, help="follow mm/dd/yyyy format. start_date <= end_date", default=None)    
    parser.add_argument('-sr', '--silent_run', type=_str_to_bool, choices=[True, False], default=True, help="Whether to show running browser. Default to hide the browser.")    
    parser.add_argument('-r', '--random_access', type=_str_to_bool, choices=[True, False], default=True, help="Whether to submit date query randomly. Default to True.")
    parser.add_argument('--save_step', type=int, default=0, help="How many pages is saved as a file for each query. Useful if scraping data in a wide date range.")
    parser.add_argument('--export_path', type=_expand_path, default=DEFAULT_EXPORT_PATH, help="The root path of exporting data.")    
    parser.add_argument('--config_path', type=_expand_path, default=DEFAULT_CONFIG_PATH, help="The path to store configurations.")    
    parser.add_argument('--scheduled_date', type=str, default=None, help="The scheduled date to start scraping. Start immediately if not provided. Follows mm/dd/YYYY format.")    
    parser.add_argument('--scheduled_time', type=str, default=None, help="The scheduled time to start scraping. If date provided, start at 12:00 am if the time is not provided. Follows military time format with hh:mm.")    
    parser.add_argument('--sleep_per_step', type=int, default=None, help="How many days of data can be scraped before a sleep. Each day is a step.")    
    parser.add_argument('--min_sleep_time', type=_str_to_seconds, default=None, help="How much time to sleep at minimum. Follows time format with hh:mm:ss.")    
    parser.add_argument('--max_sleep_time', type=_str_to_seconds, default=None, help="How much time to sleep at maximum. Follows time format with hh:mm:ss.")    

    args = parser.parse_args()

    return args


def main(args, daily_feed=False):
    global CURRENT_TIME
    CURRENT_TIME = datetime.datetime.now().strftime("%Y-%m-%d_%H%M%S")
    start_t = time.time()

    export_path = args['export_path']
    config_path = args['config_path']
    hide_browser = args['silent_run']

    if daily_feed:
        # Routine daily feed mode
        # Run at midnight to get full data

        with open(config_path+'daily_feed.json', 'r') as f:
            feed_configs = json.load(f)

        for database in feed_configs['db']:
            for market_category in feed_configs['category']:
                print("Performing daily data update for {} market of {}".format(market_category, database))

                db_name = database
                status = feed_configs['status']
                category = market_category
                historical_end_date = args['start_date']

                yesterday = datetime.datetime.now() - datetime.timedelta(days=1)
                start_day = yesterday.strftime("%m/%d/%Y")
                end_day = yesterday.strftime("%m/%d/%Y")
                
                if args['save_step'] > 0:
                    data_set_name = '{}_{}_{}_query_{}_p'.format(db_name, category, status, CURRENT_TIME)
                    print("Partial save name {}".format(data_set_name))
                else:    
                    data_set_name = '{}_{}_{}_query_{}.csv'.format(db_name, category, status, CURRENT_TIME)
                
                dbscraper = DBScraper(db_name, config_path, export_path, data_set_name, hide_browser)
                dbscraper.start_scrape(
                    market_category, 
                    [], 
                    status,
                    end_day,
                    start_day,
                    save_step=args['save_step']
                )

    else:    
        db_name = args['db']
        status = args['status']
        category = args['market']


        # we get data by go back in time
        historical_end_date = args['start_date']
        start_day = args['end_date']

        pp.pprint("Start from {}".format(historical_end_date))
        pp.pprint("End at {}".format(start_day))

        if args['save_step'] > 0:
            data_set_name = '{}_{}_{}_query_{}_p'.format(db_name, category, status, CURRENT_TIME)
            print("Partial save name {}".format(data_set_name))
        else:    
            data_set_name = '{}_{}_{}_query_{}.csv'.format(db_name, category, status, CURRENT_TIME)
    
        dt_hist_end_date = datetime.datetime.strptime(historical_end_date, "%m/%d/%Y")
        dt_hist_start_date = datetime.datetime.strptime(start_day, "%m/%d/%Y")

        if dt_hist_end_date < dt_hist_start_date:
            dbscraper = DBScraper(db_name, config_path, export_path, data_set_name, hide_browser)
            dbscraper.start_scrape(
                category, 
                [], 
                status,
                historical_end_date,
                start_day,
                save_step=args['save_step'],
                sleep_step=args['sleep_per_step'], 
                sleep_time_range=(args['min_sleep_time'], args['max_sleep_time'])
            )
        else:
            print("Start date must be smaller that end date!")

    print("Scraping takes {}".format(time.strftime("%H:%M:%S", time.gmtime(time.time() - start_t))))

    ################# Remove before delivery ####################

    if platform.system() == "Linux" and 'dbscraper' in locals():
        dbscraper.auto_upload()

    #############################################################

def stand_by():
    while True:
        schedule.run_pending()
        time.sleep(1) # wait one minute

if __name__ == '__main__':
    args = setup_args().__dict__
    # pp.pprint(args)
    
    if args['daily_active']:
        if args['scheduled_time'] is None:
            schedule.every().day.at("00:01").do(main, args, True)
            stand_by()
        else:
            if re.match('\d\d:\d\d', args['scheduled_time']):
                current = datetime.datetime.now().strftime("%H:%M:%S")
                print("Scheduled to pull active data at {}. Current time {}".format(args['scheduled_time'], current))
                scheduled_run_time = args['scheduled_time']
                schedule.every().day.at(scheduled_run_time).do(main, args, True)
                stand_by()
            else:
                print("You entered invalid time {}! Time must conform hh:mm format.".format(args['scheduled_time']))
    else:
        main(args)













