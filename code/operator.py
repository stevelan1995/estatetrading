import json
import os
import os.path as path
import time
import glob
import shutil
import selenium.webdriver.support.ui as ui
from selenium.webdriver.common.keys import Keys
from selenium import webdriver
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException, StaleElementReferenceException, ElementNotVisibleException

import pprint
pp = pprint.PrettyPrinter(indent=4)

from code.query import Query

class Operator(object):
    """ Operator does most of the hard work of operating the browser for us.
    It has a query object to help it perform searches given by us.s


    """
    def __init__(self, db_name, config_path, export_path, headless=False, run_remotely=False):
        self.config_path = config_path
        self.db_name = db_name
        self.export_path = export_path
        self.headless = headless
        self.run_remotely = run_remotely

        self._init_browser()
        self._init_config()

        self.query_engine = Query(self.browser, config_path)
        self.query_engine.set_db(self.db_name)
        self.total_pages = int(self.interactive_sess_data['total_pages'])

    def _init_config(self):
        with open(self.config_path+'db.json', 'r') as f:
            self.db_session_data = json.load(f)

        with open(self.config_path+'sess_info.json', 'r') as f:
            self.interactive_sess_data = json.load(f)

    def _init_browser(self):
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--disable-extensions')
        chrome_options.add_argument("--incognito")
        chrome_options.add_argument("--disable-plugins-discovery");
        
        if self.headless:
            chrome_options.add_argument('--headless')

        if self.run_remotely:
            self.browser = webdriver.Remote(
                command_executor='http://52.14.52.242:4444/wd/hub',
                desired_capabilities=chrome_options.to_capabilities()
            )

        self.browser = webdriver.Chrome(chrome_options=chrome_options)  

    def _click(self, element):
        self.browser.execute_script("arguments[0].click();", element)

    def log_off(self):
        logoff_btn_found = False
        logout_panel_found = False
        print("Logout account to sleep.")
        time.sleep(1)

        if self.db_name == 'philadelphia':
            while not logout_panel_found:
                try:
                    time.sleep(1)
                    log_off_btn = self.browser.find_element_by_xpath(self.current_session['log_out_panel'])
                    log_off_btn.click()
                    time.sleep(1)
                    logout_panel_found = True
                except NoSuchElementException:
                    print("Cannot find logoff button")
                    self.browser.refresh()
                    time.sleep(2.5)
                except Exception:
                    print("Logoff button not found. Exception: {}".format(Exception))

        while not logoff_btn_found:
            try:
                time.sleep(1)
                log_off_btn = self.browser.find_element_by_xpath(self.current_session['log_out'])
                log_off_btn.click()
                time.sleep(1)
                logoff_btn_found = True
            except NoSuchElementException:
                print("Cannot find logoff button")
                self.browser.refresh()
                time.sleep(2.5)
            except Exception:
                print("Logoff button not found. Exception: {}".format(Exception))


    def login(self):
        print("Log in to the {} database".format(self.db_name))
        self.current_session = self.interactive_sess_data[self.db_name]

        # automatically login to fmls database 
        self.current_db_session = self.db_session_data[self.db_name]
        login_data = self.current_db_session['login_info']

        self.browser.get(self.current_db_session['url'])
        username_input = self.browser.find_element_by_id(login_data["username_input_id"])
        
        if self.db_name == 'atlanta':
            passwd_input = self.browser.find_element_by_name(login_data["passwd_input_id"])
        else:
            passwd_input = self.browser.find_element_by_id(login_data["passwd_input_id"])
        
        if login_data['btn_locating_method'] == 'tag':
            login_button = self.browser.find_element_by_tag_name('button')
        elif login_data['btn_locating_method'] == 'id':
            login_button = self.browser.find_element_by_id(login_data["login_button"])
        
        # Enter username and password
        username_input.send_keys(login_data['username'])
        passwd_input.send_keys(login_data['passwd'])
        login_button.click()
        
        if self.db_name == 'south_florida':
            self.browser.switch_to_window(self.browser.window_handles[-1])

            try:
                time.sleep(0.5)
                session_used = self.browser.find_element_by_id("m_lblMessage")
                time.sleep(0.5)
                continue_login = self.browser.find_element_by_xpath(self.current_session['continue_login'])
                continue_login.click()
            except:
                return

        time.sleep(1)

        print("Login completed.")

    def access_db(self, market_type):
        print("Accessing the database.")
        if self.db_name == 'atlanta':
            time.sleep(0.5)
            product_dropdown = self.browser.find_element_by_xpath(self.current_session['product_dropdown'])
            product_dropdown.click()
            time.sleep(0.5)
            matrix_db_link = self.browser.find_element_by_xpath(self.current_session['Matrix_db'])
            matrix_db_link.click()

        self.browser.get(self.current_session['{}_search_page'.format(market_type)])
        self.browser.switch_to_window(self.browser.window_handles[-1])
        print("Search page reached.")

    def reset_search(self, market_type):
        """Reset to the search screen"""
        self.browser.get(self.current_session[market_type+"_search_page"])

    def execute_search(self, market_type, property_type, status, look_back_days=None, start_date=None, end_date=None):
        self.reset_search(market_type)
        entry_count = 0
        successful_search = False
        
        successful_search, entry_count = self.query_engine.submit_query(
            market_type, 
            property_type, 
            status, 
            look_back=look_back_days, 
            start=start_date, 
            end=end_date
        )
        
        return entry_count, successful_search
        

    def available_table_range(self):
        max_table_index = self.current_session['total_table']
        return range(0, max_table_index)

    def show_max_row(self):
        dropdown_appeared = False
        option_appeared = False
        find_dropdown_attempt = 0
        find_dropdown_attempt_threshold = 10
        find_option_attempt = 0
        find_option_attempt_threshold = 10

        while not dropdown_appeared and find_dropdown_attempt < find_dropdown_attempt_threshold:
            try:
                time.sleep(0.5)
                row_dropdown = self.browser.find_element_by_id(self.current_session['display_row_dropdown'])
                row_dropdown.click()
                time.sleep(0.5)
                dropdown_appeared = True
            except StaleElementReferenceException:
                time.sleep(0.5)
            except ElementNotVisibleException:
                time.sleep(0.5)
                self.browser.refresh()
                time.sleep(0.5)
                find_dropdown_attempt += 1
            except Exception:
                print("dropdown not found. Exception: {}".format(Exception))

        while not option_appeared and find_option_attempt < find_option_attempt_threshold:
            try:
                time.sleep(0.5)
                option = row_dropdown.find_elements_by_tag_name('option')[-1]
                option.click()
                time.sleep(0.5)
                option_appeared = True            
            except StaleElementReferenceException:                
                time.sleep(0.5)
            except Exception:
                print("Option not appeared.")
                self.browser.refresh()
                time.sleep(0.5)
                find_option_attempt += 1



    def next_page(self):
        next_page_link_appeared = False
        time_out_limit = 10
        timed_out = False

        while not (timed_out or next_page_link_appeared):
            now = time.time()

            try:
                time.sleep(0.5)
                next_page_link = self.browser.find_element_by_id(self.current_session['next_page'])
                
                if not next_page_link.is_enabled():
                    break 

                next_page_link.click()
                time.sleep(1)
                next_page_link_appeared = True
            except StaleElementReferenceException:
                timed_out = (time.time() - now) >= time_out_limit
                time.sleep(0.5)
            # except Exception:
            #     print("Next page link not found. Try again.")
            #     self.browser.refresh()
            #     time.sleep(0.5)
            #     timed_out = (time.time() - now) >= time_out_limit

    def _get_var_table(self, table_number):
        """ Switch from table X1 to XN to include all the variables

        table_number (int): the index of variable table to display
        """
        display_dropdown_appeared = False
        option_appeared = False
        attempt = 0
        attempt_threshold = 100
        find_dropdown_attempt = 0
        find_dropdown_attempt_threshold = 100

        print("Scraping table {}".format(table_number))

        while not display_dropdown_appeared and find_dropdown_attempt < find_dropdown_attempt_threshold: 
            try:
                time.sleep(0.8)
                display_dropdown = self.browser.find_element_by_xpath(self.current_session['display_dropdown'])
                display_dropdown.click()
                time.sleep(0.8)
                display_dropdown_appeared = True
                attempt = 0

            except StaleElementReferenceException:
                time.sleep(0.1)
            except ElementNotVisibleException:
                time.sleep(0.5)
                self.browser.refresh()
                time.sleep(0.5)
                find_dropdown_attempt += 1
            except Exception:
                print("Display dropdown not found.")
                time.sleep(0.5)
                self.browser.refresh()
                time.sleep(0.5)

        while not option_appeared and attempt < attempt_threshold:
            try:
                time.sleep(0.8)
                dropdown_options = display_dropdown.find_elements_by_class_name(self.current_session["customized_dropdown_class"])    
                opt = dropdown_options[table_number]
                opt.click()
                time.sleep(0.8)
                option_appeared = True
                attempt = 0

            except StaleElementReferenceException:
                # self.browser.refresh()
                time.sleep(0.5)
                attempt += 1
            except ElementNotVisibleException:
                time.sleep(0.5)
                self.browser.refresh()
                time.sleep(0.5)
                attempt += 1
            except Exception:
                print("Option dropdown not found.")
                time.sleep(0.5)
                self.browser.refresh()
                time.sleep(0.5)
                attempt += 1

        time.sleep(1)
        page_src = self.browser.page_source
        
        return page_src

    def get_full_page(self):
        """ A wrapper of _get_var_table. Returns a page of full variable tables.

        """
        page = []

        for i in self.available_table_range():
            time.sleep(1)
            page.append(self._get_var_table(i))

        return page

