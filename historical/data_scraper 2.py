"""
Data Scraping System Prototype: Scraping three FMLS databases

This prototype consists of three modules:
1. Operator
2. Scraper
3. Query

Operator simulates human operation on the self.browser and is responsible for submiting query and
browsing pages.

Scraper takes a data listing page and scrape data from it

Query is responsible for choosing the correct filter. 

Note: Aggressive scraping may alert the database administrator

Scraping the whole database cannot be done at once, and needs to be broken 
down into sessions. Each session simulates browsing the database by n minutes.
Each session is stored as a session checkpoint, and the scraper can start
from the lastest checkpoint to continue scraping. Scraping operation should be
as stealthy as possible. 

By default, data scraper updates the last checkpoint every one minute so it can 
recover from the past one minute if get interrupted for any reason.

"""

import argparse

############## Scraper's import ##############

import re
import os
import codecs
import pprint
import requests

import os.path as path
import pandas as pd
import numpy as np
from bs4 import BeautifulSoup
from collections import OrderedDict
from pandas import DataFrame
from time import gmtime, strftime
from selenium.common.exceptions import NoSuchElementException, StaleElementReferenceException

##############################################

################## Operator's Import ################

import json
import os
import os.path as path
import time
import glob
import shutil
import selenium.webdriver.support.ui as ui
from selenium.webdriver.common.keys import Keys
from selenium import webdriver
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By

import pprint
pp = pprint.PrettyPrinter(indent=4)

#######################################################
import schedule
import datetime
import numpy as np
import subprocess
from subprocess import call
import platform
from apscheduler.schedulers.background import BackgroundScheduler

class Scraper:
    """docstring for Scraper"""
    def __init__(self):
        pass
    
    def _extract_column_names(self, column_container, table_data):
        """ Initialize extraction data by extracting column names with empty list.

        """
        column_header = self._extract_children(column_container, index=0)
        columns = self._extract_children(column_header, start=1)

        for col in columns:
            # print(col.text)
            if col != u'\xa0' and col !='\n' and col != " ":
                table_data[col.text] = []
            else:
                print("Found hidden invalid character!")

        return table_data

    def _fill_table_data(self, table_body, table_data):

        for row in table_body.children:
            data_entries = [e for e in self._extract_children(row, start=1) if e != '\n']

            for key, entry in zip(table_data.keys(), data_entries):
                if entry.text == '':
                    table_data[key].append(np.nan)
                else:
                    table_data[key].append(entry.text)

        return table_data

    def _extract_children(self, element, index=None, start=None, end=None):
        """ Extract the children of the provided element.

        If index, start, and end are all none, element itself is returned.
        If only index is given, it is equivalent to element.children[index].
        If either start or end is given, it is equivalent to element.children[start:end]
        Index and start, end cannot be given at the same time, otherwise it 
        is equivalent to the first case.

        """

        if (index is None and start is None and end is None) or \
           (index is not None and start is not None) or \
           (index is not None and end is not None):
            return element
        elif start is not None and end is not None:
            return list(element.children)[start: end]
        elif start is not None:
            return list(element.children)[start: ]
        elif end is not None:
            return list(element.children)[:end]
        elif index is not None:
            return list(element.children)[index]
        else:
            return element


    def _extract_variable_table(self, page_source):
        """ Extract variables from raw html containing variable tables like X1.
        Returns a dataframe representation. 

        """
        table_data = OrderedDict()
        soup = BeautifulSoup(page_source, "lxml")

        data_area = self._extract_children(soup.find(id="m_pnlDisplay"), index=1)
        header_container, table_body = self._extract_children(data_area, start=1)
        table_data = self._extract_column_names(header_container, table_data)
        table_data = self._fill_table_data(table_body, table_data)
        table_data_df = DataFrame(table_data, index=None)
        
        return table_data_df
 
    def _drop_duplicated(self, df):
        """ Assuming the dataframe somehow contrains duplicates that looks like
        something.1. Drop it. Also it should not contain illegal characters.

        """

        for col_name in df.columns:
            if re.findall('\.\d', col_name):
                df = df.drop(col_name, axis=1)
            elif col_name == u'\xa0':
                df = df.drop( u'\xa0', axis=1)
                # df = df.dropna(axis=1, how='all')
                # print(df)

        return df

    def extract_full_page(self, raw_variable_table):
        """ Extract a full page of variable tables containing about 250-280 variables.
        Takes a list of raw html containing all the variables and convert into 
        dataframe.

        """
        full_page = []

        for raw_variable in raw_variable_table:
            var_table_df = self._extract_variable_table(raw_variable)
            full_page.append(var_table_df)

        # Concat by column, and perform sanity check.
        full_page_df = self._drop_duplicated(pd.concat(full_page, axis=1))

        return full_page_df


class Query():
    """Query helps us to submit basic searches to narrow down
    the range of data. The very first version should be able to
    go to atlanta FMLS and select either residential or rental, and
    select one of two house types, then select any one of the status.
    Before executing a search, it should enter two given dates.

    In the begining, query methods are all specialized action sequences to
    perform some specific searches. Later on it can be generalized. It should
    be compatible with all three MLSs.
    """

    def __init__(self, browser, config_path):
        self.browser = browser
        self.db_name = ""
        self._init_query_config(config_path)


    def _init_query_config(self, config_path):
        with open(config_path+'query_info.json', 'r') as f:
            self.db_session_data = json.load(f)

    def set_db(self, db):
        self.db_name = db


    def _uncheck_default_status(self, market_type):
        """ The Philly database has default status checked. We don't want that.

        """
        op_successful = True
        default_active_status_found = False
        default_active_no_show_status_found = False

        if not (self.db_name == 'atlanta' and market_type == 'residential'):
            while not default_active_status_found:
                try:
                    time.sleep(0.5)
                    default_active = self.browser.find_element_by_xpath(self.current_session[market_type+"_active"])
                    default_active.click()
                    time.sleep(0.5)
                    default_active_status_found = True
                except NoSuchElementException:
                    print("Warning: we are probably not on the right page. Element no found.")
                    # self.browser.refresh()
                    time.sleep(0.5)
                except Exception:
                    print("Something is wrong. Skip and reinitiate search. Exception: {}".format(Exception))
                    op_successful = False
                    return op_successful

        if self.db_name == 'philadelphia':
            while not default_active_no_show_status_found:
                try:
                    time.sleep(0.5)
                    default_active_no_show = self.browser.find_element_by_xpath(self.current_session[market_type+"_active_no_showing"])
                    default_active_no_show.click()
                    time.sleep(0.5)
                    default_active_no_show_status_found = True
                except NoSuchElementException:
                    print("Warning: we are probably not on the right page. Element no found.")
                    # self.browser.refresh()
                    time.sleep(0.5)
                except Exception:
                    print("Something is wrong. Skip and reinitiate search. Exception: {}".format(Exception))
                    op_successful = False
                    return op_successful

        return op_successful


    def submit_query(self, market_type, property_type, status, look_back=None, start=None, end=None):
        # go to search tab
        self.current_session =  self.db_session_data[self.db_name]
        query_successful = True

        search_tab_found = False
        search_page_found = False
        status_found = False
        status_date_range_found = False
        search_btn_found = False

        self.browser.get(self.current_session[market_type+"_search_page"])
        time.sleep(1)

        success = self._uncheck_default_status(market_type)

        if not success:
            return False

        while not status_found:
            try:
                if market_type+'_'+status in self.current_session:
                    time.sleep(0.5)
                    status_chk = self.browser.find_element_by_xpath(self.current_session[market_type+'_'+status])
                    status_chk.click()
                    time.sleep(0.5)
                    status_found = True
                else:
                    print("Incorrect query: requested market_type or status not available for this region.")
                    query_successful = False
                    return query_successful

            except NoSuchElementException:
                print("Warning: we are probably not on the right page. Element no found.")
                time.sleep(1)
            except Exception:
                print("Status not found. Skip and reinitiate search. Exception: {}".format(Exception))
                query_successful = False
                return query_successful

        while not status_date_range_found:
            try:
                time.sleep(0.5)
                status_date_range = self.browser.find_element_by_xpath(self.current_session[market_type+'_'+status+"_input"])
                status_date_range.clear()
                status_date_range_found = True
            except NoSuchElementException:
                print("Warning: we are probably not on the right page. Element no found.")
                time.sleep(0.5)
            except Exception:
                print("Status date range not found. Skip and reinitiate search. Exception: {}".format(Exception))
                query_successful = False
                return query_successful

        
        if start is not None and end is not None:
            date_str = "{}".format(end_date.replace('-', '/'))
            status_date_range.send_keys(date_str)
        elif look_back is not None:
            # print(look_back, type(look_back))
            status_date_range.send_keys(str(look_back.days))
        else:
            pass

        time.sleep(1)

        while not search_btn_found:
            try:
                time.sleep(0.5)
                submit_search = self.browser.find_element_by_id(self.current_session["submit_search"])
                submit_search.click()
                time.sleep(1)
                search_btn_found = True
            except NoSuchElementException:
                print("Warning: we are probably not on the right page. Element no found.")
                # self.browser.refresh()
                time.sleep(1)
            except Exception:
                print("Search button not found. Skip and reinitiate search. Exception: {}".format(Exception))
                query_successful = False
                return query_successful
        
        return query_successful

    def show_entry_count(self):
        element_found = False
        attempt = 0
        threshold = 10
        entry_count = 0

        # if element not found, try to redo the search
        while attempt < threshold and not element_found :
            try:
                time.sleep(1)
                q_count = self.browser.find_element_by_xpath(self.current_session['query_count']).text
                entry_count = int(re.findall('\d+', q_count)[0])
                element_found = True
            except NoSuchElementException:
                print("Warning: we are probably not on the right page. Element no found.")
                time.sleep(1)
                attempt += 1


        return entry_count


class Operator(object):
    """ Operator does most of the hard work of operating the browser for us.
    It has a query object to help it perform searches given by us.s


    """
    def __init__(self, db_name, config_path, export_path, headless=False, run_remotely=False):
        self.config_path = config_path
        self.db_name = db_name
        self.export_path = export_path
        self.headless = headless
        self.run_remotely = run_remotely

        self._init_browser()
        self._init_config()

        self.query_engine = Query(self.browser, config_path)
        self.query_engine.set_db(self.db_name)
        self.total_pages = int(self.interactive_sess_data['total_pages'])

    def _init_config(self):
        with open(self.config_path+'db.json', 'r') as f:
            self.db_session_data = json.load(f)

        with open(self.config_path+'sess_info.json', 'r') as f:
            self.interactive_sess_data = json.load(f)

    def _init_browser(self):
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--disable-extensions')
        chrome_options.add_argument("--incognito")
        chrome_options.add_argument("--disable-plugins-discovery");
        
        if self.headless:
            chrome_options.add_argument('--headless')

        if self.run_remotely:
            self.browser = webdriver.Remote(
                command_executor='http://52.14.52.242:4444/wd/hub',
                desired_capabilities=chrome_options.to_capabilities()
            )

        self.browser = webdriver.Chrome(chrome_options=chrome_options)  

    def login(self):
        print("log in to the database")
        self.current_session = self.interactive_sess_data[self.db_name]

        # automatically login to fmls database 
        self.current_db_session = self.db_session_data[self.db_name]
        login_data = self.current_db_session['login_info']

        self.browser.get(self.current_db_session['url'])
        username_input = self.browser.find_element_by_id(login_data["username_input_id"])
        
        if self.db_name == 'atlanta':
            passwd_input = self.browser.find_element_by_name(login_data["passwd_input_id"])
        else:
            passwd_input = self.browser.find_element_by_id(login_data["passwd_input_id"])
        
        if login_data['btn_locating_method'] == 'tag':
            login_button = self.browser.find_element_by_tag_name('button')
        elif login_data['btn_locating_method'] == 'id':
            login_button = self.browser.find_element_by_id(login_data["login_button"])
        
        # Enter username and password
        username_input.send_keys(login_data['username'])
        passwd_input.send_keys(login_data['passwd'])
        login_button.click()
        
        if self.db_name == 'south_florida':
            self.browser.switch_to_window(self.browser.window_handles[-1])

            try:
                time.sleep(0.5)
                session_used = self.browser.find_element_by_id("m_lblMessage")
                time.sleep(0.5)
                continue_login = self.browser.find_element_by_xpath(self.current_session['continue_login'])
                continue_login.click()
            except:
                return

        time.sleep(1)

        print("Login completed.")

    def access_db(self, market_type):
        print("Accessing the database.")
        if self.db_name == 'atlanta':
            time.sleep(0.5)
            product_dropdown = self.browser.find_element_by_xpath(self.current_session['product_dropdown'])
            product_dropdown.click()
            time.sleep(0.5)
            matrix_db_link = self.browser.find_element_by_xpath(self.current_session['Matrix_db'])
            matrix_db_link.click()

        self.browser.get(self.current_session['{}_search_page'.format(market_type)])
        self.browser.switch_to_window(self.browser.window_handles[-1])
        print("Search page reached.")

    def reset_search(self, market_type):
        """Reset to the search screen"""
        self.browser.get(self.current_session[market_type+"_search_page"])

    def execute_search(self, market_type, property_type, status, look_back_days=None, start_date=None, end_date=None):
        self.reset_search(market_type)
        entry_count = 0
        successful_search = False
        
        successful_search = self.query_engine.submit_query(
            market_type, 
            property_type, 
            status, 
            look_back=look_back_days, 
            start=start_date, 
            end=end_date
        )
        
        if successful_search:
            time.sleep(0.5)
            entry_count = self.query_engine.show_entry_count()
            
            if entry_count > 0:
                time.sleep(1)
                search_btn = self.browser.find_element_by_xpath(self.current_session["search_button"])
                search_btn.click()

        return entry_count, successful_search
        

    def available_table_range(self):
        max_table_index = self.current_session['total_table']
        return range(0, max_table_index)

    def show_max_row(self):
        dropdown_appeared = False
        option_appeared = False

        while not dropdown_appeared:
            try:
                time.sleep(0.5)
                row_dropdown = self.browser.find_element_by_id(self.current_session['display_row_dropdown'])
                row_dropdown.click()
                time.sleep(0.5)
                dropdown_appeared = True
            except StaleElementReferenceException:
                time.sleep(0.5)

        while not option_appeared:
            try:
                time.sleep(0.5)
                option = row_dropdown.find_elements_by_tag_name('option')[-1]
                option.click()
                time.sleep(0.5)
                option_appeared = True            
            except StaleElementReferenceException:                
                time.sleep(0.5)


    def next_page(self):
        next_page_link_appeared = False
        time_out_limit = 10
        timed_out = False

        while not (timed_out or next_page_link_appeared):
            now = time.time()

            try:
                time.sleep(0.5)
                next_page_link = self.browser.find_element_by_id(self.current_session['next_page'])
                
                if not next_page_link.is_enabled():
                    break 

                next_page_link.click()
                time.sleep(1)
                next_page_link_appeared = True
            except StaleElementReferenceException:
                timed_out = (time.time() - now) >= time_out_limit
                time.sleep(0.5)

    def _get_var_table(self, table_number):
        """ Switch from table X1 to XN to include all the variables

        table_number (int): the index of variable table to display
        """
        display_dropdown_appeared = False
        option_appeared = False
        attempt = 0
        attempt_threshold = 100

        print("Scraping table {}".format(table_number))

        while not display_dropdown_appeared:
            try:
                time.sleep(0.8)
                display_dropdown = self.browser.find_element_by_xpath(self.current_session['display_dropdown'])
                display_dropdown.click()
                time.sleep(0.8)
                display_dropdown_appeared = True
            except StaleElementReferenceException:
                time.sleep(0.1)
        
        while not option_appeared:
            if attempt >= attempt_threshold:
                break

            try:
                time.sleep(0.8)
                dropdown_options = display_dropdown.find_elements_by_class_name(self.current_session["customized_dropdown_class"])    
                opt = dropdown_options[table_number]
                opt.click()
                time.sleep(0.8)
                option_appeared = True
            except StaleElementReferenceException:
                # self.browser.refresh()
                time.sleep(0.5)
                attempt += 1

        time.sleep(1)
        page_src = self.browser.page_source
        
        return page_src

    def get_full_page(self):
        """ A wrapper of _get_var_table. Returns a page of full variable tables.

        """
        page = []

        for i in self.available_table_range():
            time.sleep(1)
            page.append(self._get_var_table(i))

        return page




class DBScraper():
    """ Main driver class for scraping MLS databases.

    """

    def __init__(self, db_name, config_path, export_path, dataset_name, hide_browser):
        self.db_name = db_name
        self.operator = Operator(db_name, config_path, export_path, hide_browser)
        self.scraper = Scraper()
        self._init_config(config_path)
        self.export_path = export_path
        self.dataset_name = dataset_name
        self.total_lines = 0
        self.total_variables = self.interactive_sess_data[db_name]["total_variables"]

    def _init_config(self, config_path):
        with open(config_path+'sess_info.json', 'r') as f:
            self.interactive_sess_data = json.load(f)

    def save_result(self):
        if not path.exists(self.export_path):
            os.makedirs(self.export_path)

        self.full_merged_query.to_csv(self.export_path+self.dataset_name, index=False)
        print("Data Saved!")

        # Perform a simple sanity check to see whether dimension matches up
        query_dim = self.full_merged_query.shape
        expected_dim = (self.total_lines, self.total_variables)

        if query_dim != expected_dim:
            print("Expected dimension is {}.\nActual dimesion is {}".format(query_dim, expected_dim))


    def save_html(self, page_src, page_name, table_number):
        """ Save html file for testing and analysis purposes"""
        if not path.isdir(self.export_path+page_name+'/'):
            os.makedirs(self.export_path+page_name+'/')

        with open(self.export_path+page_name+'/'+table_number+'.html', "w") as page_file:
            print("write {} to {}".format(table_number+'.html', self.export_path+page_name+'/'))
            page_file.write(page_src)


    def _concat_pages(self, page_dfs):
        """ Take a list of dfs containing pages 
        and merge each df to one df by row. One query should
        return a df shape about (5000, 260).

        Pandas sanity check enabled for testing purposes. 
        For now, there is no absolute garuntee about the correctness
        of the data even though I spend a lot effort to reach this objective.
        """
        self.full_merged_query = pd.concat(page_dfs, axis=0, ignore_index=True, verify_integrity=True)

        try:
            self.full_merged_query = pd.concat(page_dfs, axis=0, ignore_index=True, verify_integrity=True)
        except ValueError:
            self.full_merged_query = pd.concat(page_dfs, axis=0, ignore_index=True)
            # print("Integrity test failed. Check data manually")

    def _drop_duplicated(self, df):
        """ Assuming the dataframe somehow contrains duplicates that looks like
        something.1. Drop it. Also it should not contain illegal characters.

        """

        for col_name in df.columns:
            if re.findall('\.\d', col_name):
                df = df.drop(col_name, axis=1)
            elif col_name == '\xa0':
                df = df.drop(col_name, axis=1)

        df = df.drop_duplicates()
        # print(df.shape)

        return df

    def update_export_path(self, db_name, market_type):
        query_folder = "query_{}".format(CURRENT_TIME)
        self.export_path = self.export_path+"{}/{}/{}/".format(db_name, market_type, query_folder)
        print("Data is saved at {}".format(self.export_path))
        
        if not path.isdir(self.export_path):
            os.makedirs(self.export_path)


    def _create_fail_safe(self):
        # build a temporary cache folder to be more robust against network error
        current_time = datetime.datetime.now().strftime("%Y%m%d_%H%M%S")
        if not path.isdir(self.export_path+'cache/'+current_time+"_cache"+'/'):
            os.makedirs(self.export_path+'cache/'+current_time+"_cache"+'/')
            self.cache_folder = self.export_path+'cache/'+current_time+"_cache"+'/'
            print("Page caches are stored at {}".format(self.cache_folder))
        
        if not path.isdir(self.export_path+'abnormal_files/'+current_time+"_error"+'/'):
            os.makedirs(self.export_path+'abnormal_files/'+current_time+"_error"+'/')
            self.abnormal_folder = self.export_path+'abnormal_files/'+current_time+"_error"+'/'
            print("Dirty fail safe backups are stored at {}".format(self.abnormal_folder))

    def start_scrape(self, market_type, property_types, status, start_date, end_date):
        """ Constant online scraping
        From the last day to go to start day. Each query contains one day of data.

        """
        search_attempt = 0
        max_retry = 10

        self.operator.login()
        self.operator.access_db(market_type)
        date_range = pd.date_range(start=start_date, end=end_date)

        print("Today is {}".format(strftime("%m/%d/%Y", gmtime())))
        print("Scraping date from {} to {}".format(end_date, start_date))
        
        full_query_data = []

        self.update_export_path(self.db_name, market_type)
        self._create_fail_safe()

        # enable random day access
        random_date_index = np.random.randint(0, len(date_range), size=len(date_range))


        for day_idx in random_date_index:
            day = date_range[day_idx]
            print("\nScraping {} data of status {} on day {}".format(market_type, status, day))
            look_back_days = datetime.datetime.now() - day

            while search_attempt < max_retry and not search_successful:
                entries_found, search_successful = self.operator.execute_search(market_type, property_types, status, look_back_days=look_back_days)
                search_attempt += 1

            if search_successful:
                self.total_lines += entries_found
                print("Updated {} entries. We have {} entries now.".format(entries_found, self.total_lines))

                if not entries_found:
                    print("No entry returned for this search.")
                else:    
                    self.operator.show_max_row()
                    page_ends = False

                    x = 0
                    total_rows = 0
                    while not page_ends:
                        print("Scraping page {}".format(x))

                        try:
                            column_match = False
                            while not column_match:
                                raw_variable_tables = self.operator.get_full_page()  
                                page_df = self.scraper.extract_full_page(raw_variable_tables)        
                            
                                if page_df.shape[1] == self.total_variables:
                                    d_end = day.strftime("%Y-%m-%d_%H-%M-%S")
                                    page_df.to_csv(self.cache_folder+"{}_p{}.csv".format(d_end, x), index=False)                
                                    print("Saved page {} to {}\n".format(x, self.cache_folder))

                                    full_query_data.append(page_df)

                                    column_match = True
                                else:
                                    d_end = day.strftime("%Y-%m-%d_%H-%M-%S")
                                    page_df.to_csv(self.abnormal_folder+"{}_p{}.csv".format(time.time(), x), index=False)                
                                    print("Saved dirty page {} to {}\n".format(x, self.abnormal_folder))

                                if not column_match:
                                    # print("Validating column number: set(columns):{} == n_columns:{} : {}".format(len(set(page_df.columns)), len(page_df.columns), len(set(page_df.columns)) == page_df.shape[1]))
                                    print("Columns do not match with expected. Try again. Expected {}, Actual {}\n".format(self.total_variables, page_df.shape[1]))


                            print("Page {} has dimension {}\n".format(x, page_df.shape))
                            
                            x += 1
                            total_rows += page_df.shape[0]

                            self.operator.next_page()
                        except NoSuchElementException:
                            page_ends = True

                    print("There are {} rows in the extracted data. Expected {}".format(sum([d.shape[0] for d in full_query_data]), self.total_lines))
        
        if len(full_query_data) > 0:
            full_query_data = [self._drop_duplicated(df) for df in full_query_data]
            self._concat_pages(full_query_data)
            self.save_result()
        else:
            print("This query returns no result. Please check your query.")

    def _subprocess_cmd(self, command):
        process = subprocess.Popen(command, stdout=subprocess.PIPE, shell=True)
        proc_stdout = process.communicate()[0].strip()
        print(proc_stdout)

    def auto_upload(self):
        """ For linux only because we don't have official google drive support
        Remove before delivery.

        """
        cmd = "cd ~/Desktop/mls_data/; grive"
        self._subprocess_cmd(cmd)

def setup_args():
    def _str_to_bool(s):
        """Convert string to bool (in argparse context)."""
        if s.lower() not in ['true', 'false']:
            raise ValueError('Argument needs to be a '
                             'boolean, got {}'.format(s))
        return {'true': True, 'false': False}[s.lower()]

    def _expand_path(s):
        return path.expanduser(s)

    status_help = """Status of the listed properties.\n
                    Atlanta: [active, cont_due_intel, cont_ko, cont_other, pending, pending_offer_approval, withdrawn, sold, expired],\n
                    Philadelphia: [active, active_no_showing, temp_off_market, settled, expired, expired_relisted, withdrawn_relisted, withdrawn, sold],\n
                    South Florida: [active, backup_contract_call, cancelled, closed_sale, rented, temp_off_market, withdrawn, expired]
                """

    DEFAULT_EXPORT_PATH = path.expanduser("~/Desktop/mls_data/estate_trading/data/")
    DEFAULT_CONFIG_PATH = path.expanduser("~/Desktop/estatetrading/configs/")

    parser = argparse.ArgumentParser(description='Scraper for extracting data from MLS databases.')
    parser.add_argument('--daily_active', type=_str_to_bool, choices=[True, False], default=False, help="Run a daily active feed on all databases. Can be used with scheduled time to specify the pulling time.")
    parser.add_argument('--db', type=str,
                        help='FMLS database to scrape from. Available (all lower cases): [atlanta, philadelphia, south_florida]',
                        choices=['atlanta', 'philadelphia', 'south_florida'],
                        default=None
    )
    parser.add_argument('-s', '--status', type=str, help=status_help, default=None)    
    parser.add_argument('-m', '--market', type=str, choices=['residential', 'rental'], help="Market type", default=None)    
    parser.add_argument('-sd', '--start_date', type=str, help="follow mm/dd/yyyy format. start_date <= end_date", default=None)    
    parser.add_argument('-ed', '--end_date', type=str, help="follow mm/dd/yyyy format. start_date <= end_date", default=None)    
    parser.add_argument('-sr', '--silent_run', type=_str_to_bool, choices=['True', 'False'], default=True, help="Whether to show running browser. Default to hide the browser.")    
    parser.add_argument('-r', '--random_access', type=_str_to_bool, choices=['True', 'False'], default=True, help="Whether to submit date query randomly. Default to True.")
    parser.add_argument('--export_path', type=_expand_path, default=DEFAULT_EXPORT_PATH, help="The root path of exporting data.")    
    parser.add_argument('--config_path', type=_expand_path, default=DEFAULT_CONFIG_PATH, help="The path to store configurations.")    
    parser.add_argument('--scheduled_date', type=str, default=None, help="The scheduled date to start scraping. Start immediately if not provided. Follows mm/dd/YYYY format.")    
    parser.add_argument('--scheduled_time', type=str, default=None, help="The scheduled time to start scraping. If date provided, start at 12:00 am if the time is not provided. Follows military time format with hh:mm.")    

    args = parser.parse_args()

    return args


def main(args, daily_feed=False):
    global CURRENT_TIME
    CURRENT_TIME = datetime.datetime.now().strftime("%Y-%m-%d_%H%M%S")
    start_t = time.time()

    export_path = args['export_path']
    config_path = args['config_path']
    hide_browser = args['silent_run']

    if daily_feed:
        # Routine daily feed mode
        # Run at midnight to get full data

        with open(config_path+'daily_feed.json', 'r') as f:
            feed_configs = json.load(f)

        for database in feed_configs['db']:
            for market_category in feed_configs['category']:
                print("Performing daily data update for {} market of {}".format(market_category, database))

                db_name = database
                status = feed_configs['status']
                category = market_category
                historical_end_date = args['start_date']

                yesterday = datetime.datetime.now() - datetime.timedelta(days=1)
                start_day = yesterday.strftime("%m/%d/%Y")
                end_day = yesterday.strftime("%m/%d/%Y")
        
                data_set_name = '{}_{}_{}_query_{}.csv'.format(db_name, category, status, CURRENT_TIME)
                dbscraper = DBScraper(db_name, config_path, export_path, data_set_name, hide_browser)
                dbscraper.start_scrape(
                    market_category, 
                    [], 
                    status,
                    end_day,
                    start_day
                )

    else:    
        db_name = args['db']
        status = args['status']
        category = args['market']


        # we get data by go back in time
        historical_end_date = args['start_date']
        start_day = args['end_date']

        data_set_name = '{}_{}_{}_query_{}.csv'.format(db_name, category, status, CURRENT_TIME)
    

        dbscraper = DBScraper(db_name, config_path, export_path, data_set_name, hide_browser)
        dbscraper.start_scrape(
            category, 
            [], 
            status,
            historical_end_date,
            start_day
        )

    print("Scraping takes {}".format(time.strftime("%H:%M:%S", time.gmtime(time.time() - start_t))))

    ################# Remove before delivery ####################

    if platform.system() == "Linux":
        dbscraper.auto_upload()

    #############################################################

def stand_by():
    while True:
        schedule.run_pending()
        time.sleep(1) # wait one minute

if __name__ == '__main__':
    args = setup_args().__dict__
    # pp.pprint(args)
    
    if args['daily_active']:
        if args['scheduled_time'] is None:
            schedule.every().day.at("00:01").do(main, args, True)
            stand_by()
        else:
            if re.match('\d\d:\d\d', args['scheduled_time']):
                current = datetime.datetime.now().strftime("%H:%M:%S")
                print("Scheduled to pull active data at {}. Current time {}".format(args['scheduled_time'], current))
                scheduled_run_time = args['scheduled_time']
                schedule.every().day.at(scheduled_run_time).do(main, args, True)
                stand_by()
            else:
                print("You entered invalid time {}! Time must conform hh:mm format.".format(args['scheduled_time']))
    else:
        main(args)













