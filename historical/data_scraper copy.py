"""
Data Scraping System Prototype: Scraping three FMLS databases

This prototype consists of three modules:
1. Operator
2. Scraper
3. Query

Operator simulates human operation on the self.browser and is responsible for submiting query and
browsing pages.

Scraper takes a data listing page and scrape data from it

Query is responsible for choosing the correct filter. 

Note: Aggressive scraping may alert the database administrator

Scraping the whole database cannot be done at once, and needs to be broken 
down into sessions. Each session simulates browsing the database by n minutes.
Each session is stored as a session checkpoint, and the scraper can start
from the lastest checkpoint to continue scraping. Scraping operation should be
as stealthy as possible. 

By default, data scraper updates the last checkpoint every one minute so it can 
recover from the past one minute if get interrupted for any reason.

"""

############## Scraper's import ##############

import re
import os
import codecs
import pprint
import requests

import os.path as path
import pandas as pd
import numpy as np
from bs4 import BeautifulSoup
from collections import OrderedDict
from pandas import DataFrame

##############################################

################## Operator's Import ################

import json
import os
import os.path as path
import time
import glob
import shutil
import selenium.webdriver.support.ui as ui
from selenium.webdriver.common.keys import Keys
from selenium import webdriver
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By

import pprint
pp = pprint.PrettyPrinter(indent=4)

#######################################################



class Scraper:
    """docstring for Scraper"""
    def __init__(self):
        pass
    
    def _extract_column_names(self, column_container, table_data):
        """ Initialize extraction data by extracting column names with empty list.

        """
        column_header = self._extract_children(column_container, index=0)
        columns = self._extract_children(column_header, start=1)

        for col in columns:
            table_data[col.text] = []

        return table_data

    def _fill_table_data(self, table_body, table_data):
        # pp.pprint(table_data.keys())

        for row in table_body.children:
            # Alert! Check extracted data type!
            # Data should be tags only. If you see something else, you make a mistake!
            data_entries = [e for e in self._extract_children(row, start=1) if e != '\n']
            # print(type(data_entries[0]))
            # pp.pprint(data_entries)

            for key, entry in zip(table_data.keys(), data_entries):
                # print(entry.text)
                if entry.text == '':
                    table_data[key].append(np.nan)
                else:
                    table_data[key].append(entry.text)

        return table_data

    def _extract_children(self, element, index=None, start=None, end=None):
        """ Extract the children of the provided element.

        If index, start, and end are all none, element itself is returned.
        If only index is given, it is equivalent to element.children[index].
        If either start or end is given, it is equivalent to element.children[start:end]
        Index and start, end cannot be given at the same time, otherwise it 
        is equivalent to the first case.

        """

        if (index is None and start is None and end is None) or \
           (index is not None and start is not None) or \
           (index is not None and end is not None):
            return element
        elif start is not None and end is not None:
            return list(element.children)[start: end]
        elif start is not None:
            return list(element.children)[start: ]
        elif end is not None:
            return list(element.children)[:end]
        elif index is not None:
            return list(element.children)[index]
        else:
            return element


    def _extract_variable_table(self, page_source):
        """ Extract variables from raw html containing variable tables like X1.
        Returns a dataframe representation. 

        """
        table_data = OrderedDict()
        soup = BeautifulSoup(page_source, "lxml")

        data_area = self._extract_children(soup.find(id="m_pnlDisplay"), index=1)
        header_container, table_body = self._extract_children(data_area, start=1)
        table_data = self._extract_column_names(header_container, table_data)
        table_data = self._fill_table_data(table_body, table_data)
        table_data_df = DataFrame(table_data, index=None)
        
        return table_data_df

    def extract_full_page(self, raw_variable_table):
        """ Extract a full page of variable tables containing about 250-280 variables.
        Takes a list of raw html containing all the variables and convert into 
        dataframe.

        """
        full_page = []

        for raw_variable in raw_variable_table:
            var_table_df = self._extract_variable_table(raw_variable)
            full_page.append(var_table_df)

        # Concat by column, and perform sanity check.
        # Check data immediately if pandas reports an error!
        # print(full_page)
        # full_page_df = pd.concat(full_page, axis=1, verify_integrity=True)
        full_page_df = pd.concat(full_page, axis=1)

        return full_page_df


class Query():
    """docstring for Query module"""

    def __init__():
        pass        


class Operator(object):
    """docstring for Operator"""
    def __init__(self, db_name, config_path):
        self.config_path = config_path
        self.db_name = db_name
        self._init_browser()
        self._init_config()

        self.total_pages = int(self.interactive_sess_data['total_pages'])
        # self.page_scraped = False

    def _init_config(self):
        with open(self.config_path+'db.json', 'r') as f:
            self.db_session_data = json.load(f)

        with open(self.config_path+'sess_info.json', 'r') as f:
            self.interactive_sess_data = json.load(f)

    def _init_browser(self):
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--disable-extensions')
        chrome_options.add_argument("--incognito")
        chrome_options.add_argument("--disable-plugins-discovery");
        chrome_options.add_argument("--start-maximized")
        self.browser = webdriver.Chrome(chrome_options=chrome_options)  

    def page_range(self):
        return range(0, self.total_pages-1)

    def login(self):

        # automatically login to fmls database 
        self.current_db_session = self.db_session_data[self.db_name]
        login_data = self.current_db_session['login_info']


        self.browser.get(self.current_db_session['url'])
        username_input = self.browser.find_element_by_id(login_data["username_input_id"])
        passwd_input = self.browser.find_element_by_name(login_data["passwd_input_id"])
        
        if login_data['btn_locating_method'] == 'tag':
            login_button = self.browser.find_element_by_tag_name('button')
        elif login_data['btn_locating_method'] == 'id':
            login_button = self.browser.find_element_by_id(login_data["login_button"])
        
        # Enter username and password
        username_input.send_keys(login_data['username'])
        passwd_input.send_keys(login_data['passwd'])
        login_button.click()
        time.sleep(2)

    def access_db(self):
        self.current_session = self.interactive_sess_data[self.db_name]
        product_dropdown = self.browser.find_element_by_xpath(self.current_session['product_dropdown'])
        product_dropdown.click()
        time.sleep(2)
        matrix_db_link = self.browser.find_element_by_xpath(self.current_session['Matrix_db'])
        matrix_db_link.click()

        self.browser.get(self.current_session['search_page'])
        time.sleep(1)
        self.browser.switch_to_window(self.browser.window_handles[-1])
        time.sleep(1)
       
        time.sleep(2)
        search_btn = self.browser.find_element_by_xpath(self.current_session['search_button'])
        search_btn.click()
        time.sleep(2)

    def available_table_range(self):
        max_table_index = int(self.current_session['total_table'])
        return range(1, max_table_index+1)

    def show_max_row(self):
        row_dropdown = self.browser.find_element_by_id(self.current_session['display_row_dropdown'])
        row_dropdown.click()
        row_dropdown.find_elements_by_tag_name('option')[-1].click()


    def next_page(self):
        next_page_link = self.browser.find_element_by_id(self.current_session['next_page'])
        next_page_link.click()
        time.sleep(1)

    def _get_var_table(self, table_number):
        """ Switch from table X1 to XN to include all the variables

        table_number (int): the index of variable table to display
        """
        print("Scraping table {}".format(table_number))

        display_dropdown = self.browser.find_element_by_xpath(self.current_session['display_dropdown'])
        dropdown_options = display_dropdown.find_elements_by_class_name(self.current_session["customized_dropdown_class"])

        time.sleep(1)
        display_dropdown.click()
        time.sleep(1)

        for opt in dropdown_options:
            # print("Looking at {}".format(opt.text))

            if opt.text == 'my:X%i'%table_number:
                opt.click()
                page_src = self.browser.page_source
                return page_src

        raise RunTimeError("Table not found!")

    def get_full_page(self):
        """ A wrapper of _get_var_table. Returns a page of full variable tables.

        """
        return [self._get_var_table(i) for i in self.available_table_range()]



class DBScraper():
    """ Main driver class for scraping MLS databases.

    """

    def __init__(self, db_name, config_path, export_path, dataset_name):
        self.operator = Operator(db_name, config_path)
        self.scraper = Scraper()
        self.export_path = export_path
        self.dataset_name = dataset_name

    def save_result(self):
        if not path.exists(self.export_path):
            os.makedirs(self.export_path)

        self.full_merged_query.to_csv(self.export_path+self.dataset_name, index=False)
        pp.pprint(self.full_merged_query)
        pp.pprint("Data Saved!")

    def save_html(self, page_src, page_name, table_number):
        """ Save html file for testing and analysis purposes"""
        if not path.isdir(self.export_path+page_name+'/'):
            os.makedirs(self.export_path+page_name+'/')

        with open(self.export_path+page_name+'/'+table_number+'.html', "w") as page_file:
            print("write {} to {}".format(table_number+'.html', self.export_path+page_name+'/'))
            page_file.write(page_src)

    def _concat_pages(self, page_dfs):
        """ Take a list of dfs containing pages 
        and merge each df to one df by row. One query should
        return a df shape about (5000, 260).

        Pandas sanity check enabled for testing purposes. 
        For now, there is no absolute garuntee about the correctness
        of the data even though I spend a lot effort to reach this objective.
        """

        self.full_merged_query = pd.concat(page_dfs, axis=0, ignore_index=True, verify_integrity=True)



    def start_scrape(self):
        self.operator.login()
        self.operator.access_db()
        self.operator.show_max_row()

        full_query_data = []
        
        # uncomment for full power
        for p in self.operator.page_range():
        
        # for p in range(0, 3):
            print("Scraping page {}/50".format(p))

            raw_variable_tables = self.operator.get_full_page()  
            page_df = self.scraper.extract_full_page(raw_variable_tables)        
            page_df.to_csv(self.export_path+"{}.csv".format(p))
            print("Page {} Extracted.\n{}".format(p, page_df))
            
            full_query_data.append(page_df)

            self.operator.next_page()
            time.sleep(1.5)

        self._concat_pages(full_query_data)
        self.save_result()

def main():
    db_name = 'atlanta'
    config_path = path.expanduser("~/Desktop/estatetrading/configs/")
    export_path = path.expanduser("~/Desktop/real_estate_data/csv_data/Residential/tests/")

    data_set_name = 'active_sample_full.csv'

    dbscraper = DBScraper(db_name, config_path, export_path, data_set_name)
    dbscraper.start_scrape()


if __name__ == '__main__':
    main()

