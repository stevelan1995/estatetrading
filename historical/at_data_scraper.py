"""
Data Scraping System Prototype: Scraping three FMLS databases

This prototype consists of three modules:
1. Operator
2. Scraper
3. Query

Operator simulates human operation on the self.browser and is responsible for submiting query and
browsing pages.

Scraper takes a data listing page and scrape data from it

Query is responsible for choosing the correct filter. 

Note: Aggressive scraping may alert the database administrator

Scraping the whole database cannot be done at once, and needs to be broken 
down into sessions. Each session simulates browsing the database by n minutes.
Each session is stored as a session checkpoint, and the scraper can start
from the lastest checkpoint to continue scraping. Scraping operation should be
as stealthy as possible. 

By default, data scraper updates the last checkpoint every one minute so it can 
recover from the past one minute if get interrupted for any reason.

"""

import argparse

############## Scraper's import ##############

import re
import os
import codecs
import pprint
import requests

import os.path as path
import pandas as pd
import numpy as np
from bs4 import BeautifulSoup
from collections import OrderedDict
from pandas import DataFrame
from time import gmtime, strftime
from selenium.common.exceptions import NoSuchElementException, StaleElementReferenceException

##############################################

################## Operator's Import ################

import json
import os
import os.path as path
import time
import glob
import shutil
import selenium.webdriver.support.ui as ui
from selenium.webdriver.common.keys import Keys
from selenium import webdriver
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By

import pprint
pp = pprint.PrettyPrinter(indent=4)

#######################################################

import datetime

class Scraper:
    """docstring for Scraper"""
    def __init__(self):
        pass
    
    def _extract_column_names(self, column_container, table_data):
        """ Initialize extraction data by extracting column names with empty list.

        """
        column_header = self._extract_children(column_container, index=0)
        columns = self._extract_children(column_header, start=1)

        for col in columns:
            table_data[col.text] = []

        return table_data

    def _fill_table_data(self, table_body, table_data):
        # pp.pprint(table_data.keys())

        for row in table_body.children:
            # Alert! Check extracted data type!
            # Data should be tags only. If you see something else, you make a mistake!
            data_entries = [e for e in self._extract_children(row, start=1) if e != '\n']
            # print(type(data_entries[0]))
            # pp.pprint(data_entries)

            for key, entry in zip(table_data.keys(), data_entries):
                # print(entry.text)
                if entry.text == '':
                    table_data[key].append(np.nan)
                else:
                    table_data[key].append(entry.text)

        return table_data

    def _extract_children(self, element, index=None, start=None, end=None):
        """ Extract the children of the provided element.

        If index, start, and end are all none, element itself is returned.
        If only index is given, it is equivalent to element.children[index].
        If either start or end is given, it is equivalent to element.children[start:end]
        Index and start, end cannot be given at the same time, otherwise it 
        is equivalent to the first case.

        """

        if (index is None and start is None and end is None) or \
           (index is not None and start is not None) or \
           (index is not None and end is not None):
            return element
        elif start is not None and end is not None:
            return list(element.children)[start: end]
        elif start is not None:
            return list(element.children)[start: ]
        elif end is not None:
            return list(element.children)[:end]
        elif index is not None:
            return list(element.children)[index]
        else:
            return element


    def _extract_variable_table(self, page_source):
        """ Extract variables from raw html containing variable tables like X1.
        Returns a dataframe representation. 

        """
        table_data = OrderedDict()
        soup = BeautifulSoup(page_source, "lxml")

        data_area = self._extract_children(soup.find(id="m_pnlDisplay"), index=1)
        header_container, table_body = self._extract_children(data_area, start=1)
        table_data = self._extract_column_names(header_container, table_data)
        table_data = self._fill_table_data(table_body, table_data)
        table_data_df = DataFrame(table_data, index=None)
        
        return table_data_df

    def extract_full_page(self, raw_variable_table):
        """ Extract a full page of variable tables containing about 250-280 variables.
        Takes a list of raw html containing all the variables and convert into 
        dataframe.

        """
        full_page = []

        for raw_variable in raw_variable_table:
            var_table_df = self._extract_variable_table(raw_variable)
            full_page.append(var_table_df)

        # Concat by column, and perform sanity check.
        # Check data immediately if pandas reports an error!
        # print(full_page)
        # full_page_df = pd.concat(full_page, axis=1, verify_integrity=True)
        full_page_df = pd.concat(full_page, axis=1)

        return full_page_df


class Query():
    """Query helps us to submit basic searches to narrow down
    the range of data. The very first version should be able to
    go to atlanta FMLS and select either residential or rental, and
    select one of two house types, then select any one of the status.
    Before executing a search, it should enter two given dates.

    In the begining, query methods are all specialized action sequences to
    perform some specific searches. Later on it can be generalized. It should
    be compatible with all three MLSs.
    """

    def __init__(self, browser, config_path):
        self.browser = browser
        self.db_name = ""
        self._init_query_config(config_path)


    def _init_query_config(self, config_path):
        with open(config_path+'query_info.json', 'r') as f:
            self.db_session_data = json.load(f)

    def set_db(self, db):
        self.db_name = db


    def submit_query(self, market_type, property_type, status, look_back=None, start=None, end=None):
        # go to search tab
        self.current_session =  self.db_session_data[self.db_name]
        query_successful = True

        search_tab_found = False
        search_page_found = False
        status_found = False
        status_date_range_found = False
        search_btn_found = False

        while not search_tab_found:
            try:
                search_tab = self.browser.find_element_by_xpath(self.current_session["search_tab"])
                search_tab.click()
                time.sleep(1)
                search_tab_found = True
            except NoSuchElementException:
                self.browser.refresh()
                print("Warning: we are probably not on the right page. Element no found.")
            except:
                print("Something is wrong. Skip and reinitiate search. Exception: {}".format(Exception))
                query_successful = False
                return query_successful

        while not search_page_found:
            try:
                property_search_page = self.browser.find_element_by_xpath(self.current_session[market_type])
                property_search_page.click()        
                time.sleep(1)
                search_page_found = True
            except NoSuchElementException:
                self.browser.refresh()
                print("Warning: we are probably not on the right page. Element no found.")
            except:
                print("Something is wrong. Skip and reinitiate search. Exception: {}".format(Exception))
                query_successful = False
                return query_successful

        while not status_found:
            try:
                status_chk = self.browser.find_element_by_xpath(self.current_session[status])
                status_chk.click()
                time.sleep(1)
                status_found = True
            except NoSuchElementException:
                self.browser.refresh()
                print("Warning: we are probably not on the right page. Element no found.")
            except:
                print("Something is wrong. Skip and reinitiate search. Exception: {}".format(Exception))
                query_successful = False
                return query_successful

        while not status_date_range_found:
            try:
                status_date_range = self.browser.find_element_by_xpath(self.current_session[status+"_input"])
                status_date_range.clear()
                time.sleep(1)
                status_date_range_found = True
            except NoSuchElementException:
                self.browser.refresh()
                print("Warning: we are probably not on the right page. Element no found.")
            except:
                print("Something is wrong. Skip and reinitiate search. Exception: {}".format(Exception))
                query_successful = False
                return query_successful

        
        if start is not None and end is not None:
            date_str = "{}".format(end_date.replace('-', '/'))
            status_date_range.send_keys(date_str)
        elif look_back is not None:
            # print(look_back, type(look_back))
            status_date_range.send_keys(str(look_back.days))
        else:
            pass

        time.sleep(1)

        while not search_btn_found:
            try:
                submit_search = self.browser.find_element_by_id(self.current_session["submit_search"])
                submit_search.click()
                time.sleep(1)
                search_btn_found = True
            except NoSuchElementException:
                self.browser.refresh()
                print("Warning: we are probably not on the right page. Element no found.")
            except:
                print("Something is wrong. Skip and reinitiate search. Exception: {}".format(Exception))
                query_successful = False
                return query_successful
        
        return query_successful

    def show_entry_count(self):
        element_found = False

        while not element_found:
            try:
                q_count = self.browser.find_element_by_xpath(self.current_session['query_count']).text
                entry_count = int(re.findall('\d+', q_count)[0])
                element_found = True
            except NoSuchElementException:
                self.browser.refresh()
                print("Warning: we are probably not on the right page. Element no found.")


        return entry_count


class Operator(object):
    """ Operator does most of the hard work of operating the browser for us.
    It has a query object to help it perform searches given by us.s


    """
    def __init__(self, db_name, config_path, export_path):
        self.config_path = config_path
        self.db_name = db_name
        self.export_path = export_path

        self._init_browser()
        self._init_config()

        self.query_engine = Query(self.browser, config_path)
        self.query_engine.set_db(self.db_name)
        self.total_pages = int(self.interactive_sess_data['total_pages'])

    def _init_config(self):
        with open(self.config_path+'db.json', 'r') as f:
            self.db_session_data = json.load(f)

        with open(self.config_path+'sess_info.json', 'r') as f:
            self.interactive_sess_data = json.load(f)

    def _init_browser(self):
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--disable-extensions')
        chrome_options.add_argument("--incognito")
        chrome_options.add_argument("--disable-plugins-discovery");
        chrome_options.add_argument('--headless')

        # self.browser = webdriver.Remote(
        #     command_executor='http://52.14.52.242:4444/wd/hub',
        #     desired_capabilities=chrome_options.to_capabilities()
        # )

        self.browser = webdriver.Chrome(chrome_options=chrome_options)  

    def get_updated_export_path(self):
        return self.export_path

    def login(self):
        print("loggin to the database")

        # automatically login to fmls database 
        self.current_db_session = self.db_session_data[self.db_name]
        login_data = self.current_db_session['login_info']

        self.browser.get(self.current_db_session['url'])
        username_input = self.browser.find_element_by_id(login_data["username_input_id"])
        
        if self.db_name == 'atlanta':
            passwd_input = self.browser.find_element_by_name(login_data["passwd_input_id"])
        else:
            passwd_input = self.browser.find_element_by_id(login_data["passwd_input_id"])
        
        if login_data['btn_locating_method'] == 'tag':
            login_button = self.browser.find_element_by_tag_name('button')
        elif login_data['btn_locating_method'] == 'id':
            login_button = self.browser.find_element_by_id(login_data["login_button"])
        
        # Enter username and password
        username_input.send_keys(login_data['username'])
        passwd_input.send_keys(login_data['passwd'])
        login_button.click()
        time.sleep(1)

        print("Login completed.")

    def access_db(self):
        print("Accessing the database.")
        self.current_session = self.interactive_sess_data[self.db_name]
        if self.db_name == 'atlanta':
            product_dropdown = self.browser.find_element_by_xpath(self.current_session['product_dropdown'])
            product_dropdown.click()
            time.sleep(1)
            matrix_db_link = self.browser.find_element_by_xpath(self.current_session['Matrix_db'])
            matrix_db_link.click()

        self.browser.get(self.current_session['residential_search_page'])
        time.sleep(1)
        self.browser.switch_to_window(self.browser.window_handles[-1])
        time.sleep(1)
        print("Search page reached.")

    def reset_search(self):
        """Reset to the search screen"""
        self.browser.get("https://fmls.mlsmatrix.com/Matrix/Search/Residential")

    def execute_search(self, market_type, property_type, status, look_back_days=None, start_date=None, end_date=None):
        self.reset_search()
        
        successful_search = self.query_engine.submit_query(
            market_type, 
            property_type, 
            status, 
            look_back=look_back_days, 
            start=start_date, 
            end=end_date
        )
        entry_count = 0

        if successful_search:
            time.sleep(1)
            entry_count = self.query_engine.show_entry_count()
            
            if entry_count > 0:
                search_btn = self.browser.find_element_by_xpath(self.current_session["search_button"])
                search_btn.click()
        else:
            print("Search failed.")

        return entry_count, successful_search
        

    def available_table_range(self):
        max_table_index = int(self.current_session['total_table'])
        return range(0, max_table_index)

    def show_max_row(self):
        dropdown_appeared = False
        option_appeared = False

        while not dropdown_appeared:
            try:
                row_dropdown = self.browser.find_element_by_id(self.current_session['display_row_dropdown'])
                row_dropdown.click()
                time.sleep(1)
                dropdown_appeared = True
            except StaleElementReferenceException:
                time.sleep(0.5)

        while not option_appeared:
            try:
                option = row_dropdown.find_elements_by_tag_name('option')[-1]
                option.click()
                time.sleep(1)
                option_appeared = True            
            except StaleElementReferenceException:                
                time.sleep(0.5)


    def next_page(self):
        next_page_link_appeared = False
        time_out_limit = 10
        timed_out = False

        while not (timed_out or next_page_link_appeared):
            now = time.time()

            try:
                next_page_link = self.browser.find_element_by_id(self.current_session['next_page'])
                
                if not next_page_link.is_enabled():
                    break 

                next_page_link.click()
                time.sleep(2)
                next_page_link_appeared = True
            except StaleElementReferenceException:
                print("Failed to turn to the next page. Try again.")
                timed_out = (time.time() - now) >= time_out_limit
                time.sleep(0.5)

    def _get_var_table(self, table_number):
        """ Switch from table X1 to XN to include all the variables

        table_number (int): the index of variable table to display
        """
        display_dropdown_appeared = False
        option_appeared = False
        attempt = 0
        attempt_threshold = 100

        while not display_dropdown_appeared:
            try:
                display_dropdown = self.browser.find_element_by_xpath(self.current_session['display_dropdown'])
                display_dropdown.click()
                time.sleep(0.5)
                display_dropdown_appeared = True
            except StaleElementReferenceException:
                time.sleep(0.1)
        
        while not option_appeared:
            if attempt >= attempt_threshold:
                break

            try:
                dropdown_options = display_dropdown.find_elements_by_class_name(self.current_session["customized_dropdown_class"])    
                opt = dropdown_options[table_number]
                opt.click()
                time.sleep(1)
                option_appeared = True
            except StaleElementReferenceException:
                self.browser.refresh()
                time.sleep(0.5)
                attempt += 1

        page_src = self.browser.page_source
        
        return page_src

    def get_full_page(self):
        """ A wrapper of _get_var_table. Returns a page of full variable tables.

        """
        return [self._get_var_table(i) for i in self.available_table_range()]



class DBScraper():
    """ Main driver class for scraping MLS databases.

    """

    def __init__(self, db_name, config_path, export_path, dataset_name):
        self.operator = Operator(db_name, config_path, export_path)
        self.scraper = Scraper()
        self.export_path = export_path
        self.dataset_name = dataset_name
        self.total_lines = 0
        self.total_variables = 274


    def save_result(self):
        if not path.exists(self.export_path):
            os.makedirs(self.export_path)

        self.full_merged_query.to_csv(self.export_path+self.dataset_name, index=False)
        print("Data Saved!")

        # Perform a simple sanity check to see whether dimension matches up
        query_dim = self.full_merged_query.shape
        expected_dim = (self.total_lines, self.total_variables)

        if query_dim != expected_dim:
            print("Expected dimension is {}.\nActual dimension is {}".format(query_dim, expected_dim))
            raise RuntimeWarning("The dimension of data set {} saved at {} does not matches the expected dimesion! Please clean manually.".format(self.dataset_name, self.export_path))

    def save_html(self, page_src, page_name, table_number):
        """ Save html file for testing and analysis purposes"""
        if not path.isdir(self.export_path+page_name+'/'):
            os.makedirs(self.export_path+page_name+'/')

        with open(self.export_path+page_name+'/'+table_number+'.html', "w") as page_file:
            print("write {} to {}".format(table_number+'.html', self.export_path+page_name+'/'))
            page_file.write(page_src)

    def _concat_pages(self, page_dfs):
        """ Take a list of dfs containing pages 
        and merge each df to one df by row. One query should
        return a df shape about (5000, 260).

        Pandas sanity check enabled for testing purposes. 
        For now, there is no absolute guarantee about the correctness
        of the data even though I spend a lot effort to reach this objective.
        """
        try:
            self.full_merged_query = pd.concat(page_dfs, axis=0, ignore_index=True, verify_integrity=True)
        except ValueError:
            if not path.isdir(self.export_path+'failed_concat/'+current_time+"_cache"+'/'):
                os.makedirs(self.export_path+'failed_concat/'+current_time+"_cache"+'/')
                full_path = self.export_path+'failed_concat/'+current_time+"_cache"+'/'

            for page_no, df in zip(range(len(page_dfs)), page_dfs):
                print(df.shape)
                df.to_csv(full_path+"%i.csv"%page_no)



    def start_scrape(self, market_type, property_types, status, start_date, end_date):
        """ Constant online scraping
        From the last day to go to start day. Each query contains one day of data.

        """
        self.operator.login()
        self.operator.access_db()
        date_range = pd.date_range(start=start_date, end=end_date)

        print("Today is {}".format(strftime("%m/%d/%Y", gmtime())))
        print("Scraping date from {} to {}".format(end_date,start_date))
        
        full_query_data = []

        for day in reversed(date_range):
            print("\nScraping {} data of status {} on day {}".format(market_type, status, day))
            # look_back_days = datetime.strptime(end_date, "%m/%d/%Y") - day
            look_back_days = datetime.datetime.now() - day
            entries_found, search_successful = self.operator.execute_search(market_type, property_types, status, look_back_days=look_back_days)
            
            if search_successful:
                self.total_lines += entries_found

                if not entries_found:
                    print("No entry returned for this search.")
                else:    
                    self.operator.show_max_row()
                    self.export_path = self.operator.get_updated_export_path()

                    # build a temporary cache folder to be more robust against network error
                    current_time = datetime.datetime.now().strftime("%Y-%m-%d_%H.%M.%S")
                    if not path.isdir(self.export_path+'cache/'+current_time+"_cache"+'/'):
                        os.makedirs(self.export_path+'cache/'+current_time+"_cache"+'/')
                        cache_folder = self.export_path+'cache/'+current_time+"_cache"+'/'
                    
                    if not path.isdir(self.export_path+'abnormal_files/'+current_time+"_error"+'/'):
                        os.makedirs(self.export_path+'abnormal_files/'+current_time+"_error"+'/')
                        abnormal_folder = self.export_path+'abnormal_files/'+current_time+"_error"+'/'

                    page_ends = False

                    x = 0
                    total_rows = 0
                    while not page_ends:
                        try:
                            raw_variable_tables = self.operator.get_full_page()  
                            page_df = self.scraper.extract_full_page(raw_variable_tables)        
                            
                            if page_df.shape[1] == self.total_variables:
                                d_end = day.strftime("%Y-%m-%d_%H-%M-%S")
                                page_df.to_csv(cache_folder+"{}_p{}.csv".format(d_end, x))                
                                full_query_data.append(page_df)
                            else:
                                d_end = day.strftime("%Y-%m-%d_%H-%M-%S")
                                page_df.to_csv(abnormal_folder+"{}_p{}.csv".format(time.time(), x))                


                            print("Page {} has dimension {}".format(x, page_df.shape))
                            
                            x += 1
                            total_rows += page_df.shape[0]

                            self.operator.next_page()
                        except NoSuchElementException:
                            page_ends = True
                    print("Discovered {} entries. We have {} entries now.".format(entries_found, self.total_lines))
                    print("There are {} rows in the extracted data.".format(total_rows))
 
        self._concat_pages(full_query_data)
        self.save_result()

def setup_args():
    parser = argparse.ArgumentParser(description='Scraper for extracting data from MLS databases.')
    parser.add_argument('database', metavar='N', type=int, nargs='+',
                        help='an integer for the accumulator')
    parser.add_argument('--sum', dest='accumulate', action='store_const',
                        const=sum, default=max,
                        help='sum the integers (default: find the max)')    
    args = parser.parse_args()

def main():
    global CURRENT_TIME
    CURRENT_TIME = datetime.datetime.now().strftime("%Y-%m-%d_%H:%M:%S")
    start_t = time.time()

    db_name = 'atlanta'
    config_path = path.expanduser("~/Desktop/estatetrading/configs/")
    # export_path = path.expanduser("~/Desktop/real_estate_data/csv_data/")
    # test
    export_path = path.expanduser("~/Desktop/csv_test/")

    status = 'sold'
    category = 'residential'

    # we get data by go back in time
    historical_end_date = '03/14/2018'
    start_day = '04/14/2018'

    data_set_name = 'query_{}.csv'.format(CURRENT_TIME)
    dbscraper = DBScraper(db_name, config_path, export_path, data_set_name)
    dbscraper.start_scrape(
        category, 
        [], 
        status,
        historical_end_date,
        start_day
    )

    print("Scraping takes {} seconds".format(time.time() - start_t))

if __name__ == '__main__':
    main()

